<ul class="navbar-nav mr-auto">

</ul>

<ul class="navbar-nav ml-auto">

    <li class="nav-item dropdown">

        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>

        <ion-icon name="log-in-outline"></ion-icon> Login <span class="caret"></span>

        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

            <a class="dropdown-item" href="{{ route('login') }}"><ion-icon name="person-circle-outline"></ion-icon> Student Login</a>

            <a class="dropdown-item" href="{{ route('guardian.login') }}"><ion-icon name="people-outline"></ion-icon> Parent Login</a>

            <a class="dropdown-item" href="{{ route('admin.login') }}"><ion-icon name="skull-outline"></ion-icon> Admin Login</a>

        </div>

    </li>
    
</ul>
