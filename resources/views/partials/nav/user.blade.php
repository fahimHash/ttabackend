<ul class="navbar-nav mr-auto">
    <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        <ion-icon name="medal-outline"></ion-icon> Leaderboard <span class="caret"></span>
        </a>
        
        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">
            @foreach($subjects as $subject)
                <a class="dropdown-item" href="{{route('student.leaderboard.subject.show', $subject->id )}}">{{$subject->title}} Leaderboard</a>
            @endforeach
        </div>
    </li>
</ul>

<ul class="navbar-nav ml-auto">
    <li class="nav-item dropdown">
        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
            {{ Auth::user()->name }} <span class="caret"></span>
        </a>

        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
        
            <a class="dropdown-item" href="{{ route('student.profile') }}">
                <ion-icon name="person-circle-outline"></ion-icon> Profile
            </a>

            <a class="dropdown-item" href="{{ route('logout') }}"
                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                <ion-icon name="log-out-outline"></ion-icon> Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>

            
        </div>
    </li>
</ul>


