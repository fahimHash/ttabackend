<ul class="navbar-nav ml-auto">

    <li class="nav-item dropdown">

        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"

            aria-haspopup="true" aria-expanded="false" v-pre>

            <ion-icon name="person-outline"></ion-icon> {{ Auth::guard('guardian')->user()->email }} <span class="caret"></span>

        </a>

        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">

            <a class="dropdown-item" href="{{ route('admin.logout') }}" onclick="event.preventDefault();

            document.getElementById('logout-form').submit();">

                <ion-icon name="log-out-outline"></ion-icon> Logout

            </a>

            <form id="logout-form" action="{{ route('guardian.logout') }}" method="POST"

                style="display: none;">

                @csrf

            </form>

        </div>

    </li>

</ul>





