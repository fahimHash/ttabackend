<ul class="navbar-nav mr-auto">
    
    @can('manage-point')

        <li class="nav-item dropdown">

            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"

                aria-haspopup="true" aria-expanded="false" v-pre>

                <ion-icon name="cash-outline"></ion-icon> Points <span class="caret"></span>

            </a>

            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">


                <a class="dropdown-item" href="{{ route('admin.point.newform') }}">

                    <ion-icon name="color-wand-outline"></ion-icon> {{ __('Generate') }}

                </a>

                <a class="dropdown-item" href="{{ route('admin.point.transfer.index') }}">

                    <ion-icon name="wallet-outline"></ion-icon> {{ __('Requests') }}

                </a>

            </div>

        </li>

    @endcan

    @can('manage-leaderboard')  

        <li class="nav-item dropdown">

            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"

                aria-haspopup="true" aria-expanded="false" v-pre>

                <ion-icon name="medal-outline"></ion-icon> Leaderboard <span class="caret"></span>

            </a>

            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">

                @foreach($subjects as $subject)

                    <a class="dropdown-item"

                        href="{{ route('admin.leaderboard.subject.show', $subject->id) }}">

                        {{ $subject->title }} Leaderboard

                    </a>

                @endforeach

            </div>

        </li>

    @endcan

    @can('manage-subject')  

        <li class="nav-item dropdown">

            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"

                aria-haspopup="true" aria-expanded="false" v-pre>

                <ion-icon name="book-outline"></ion-icon> Subjects <span class="caret"></span>

            </a>

            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">

                <a class="dropdown-item" href="{{ route('admin.subject.new') }}">

                    <ion-icon name="settings-outline"></ion-icon> {{ __('Manage') }}

                </a>

            </div>

        </li>

    @endcan

    @can('populate-section') 

        <li class="nav-item dropdown">

            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"

                aria-haspopup="true" aria-expanded="false" v-pre>

                <ion-icon name="easel-outline"></ion-icon> Sections <span class="caret"></span>

            </a>

            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">

                @can('manage-section') 

                    <a class="dropdown-item" href="{{ route('admin.section.newform') }}">

                        <ion-icon name="settings-outline"></ion-icon> {{ __('Manage') }}

                    </a>

                @endcan

                <!-- <div class="dropdown-divider"></div>

                <a class="dropdown-item" href="">

                    <ion-icon name="people-circle-outline"></ion-icon> {{ __('Populate') }}

                </a> -->

                

            </div>

        </li>

    @endcan

    @can('manage-student')  

        <li class="nav-item dropdown">

            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"

                aria-haspopup="true" aria-expanded="false" v-pre>

                <ion-icon name="people-outline"></ion-icon> Students <span class="caret"></span>

            </a>

            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">

                <a class="dropdown-item" href="{{ route('admin.student.new') }}">

                    <ion-icon name="person-add-outline"></ion-icon> {{ __('Add') }}

                </a>

                <a class="dropdown-item" href="{{ route('admin.students.manage') }}">

                    <ion-icon name="settings-outline"></ion-icon> {{ __('Manage') }}

                </a>

            </div>

        </li>

    @endcan

</ul>

<ul class="navbar-nav ml-auto">

    @can('manage-admin')    

        <li class="nav-item dropdown">

            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"

                aria-haspopup="true" aria-expanded="false" v-pre>

                <ion-icon name="settings-outline"></ion-icon> Settings <span class="caret"></span>

            </a>

            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">

                <a class="dropdown-item" href="{{ route('admin.settings.admin.manage') }}">

                    <ion-icon name="people-circle-outline"></ion-icon> {{ __('Manage Admins') }}

                </a>

                <a class="dropdown-item" href="{{ route('admin.settings.role.manage.select') }}">

                    <ion-icon name="briefcase-outline"></ion-icon> {{ __('Manage Roles') }}

                </a>

            </div>

        </li>

    @endcan

    <li class="nav-item dropdown">

        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"

            aria-haspopup="true" aria-expanded="false" v-pre>

            <ion-icon name="person-outline"></ion-icon> {{ Auth::guard('admin')->user()->name }} <span class="caret"></span>

        </a>

        <div class="dropdown-menu dropdown-menu-left" aria-labelledby="navbarDropdown">

            <a class="dropdown-item" href="{{ route('admin.logout') }}" onclick="event.preventDefault();

            document.getElementById('logout-form').submit();">

                <ion-icon name="log-out-outline"></ion-icon> Logout

            </a>

            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST"

                style="display: none;">

                @csrf

            </form>

        </div>

    </li>

</ul>





