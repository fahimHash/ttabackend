@extends('layouts.app')


@if(Session::has('message'))
    @section('message')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @if(Session::has('flag_wrong_creds') == 'wrong_creds')
                    <div class="alert alert-danger" role="alert">
                        <strong> <ion-icon name="close-circle-outline"></ion-icon> {{Session::get('message')}} </strong> 
                    </div>
                @endif

                @if(Session::has('flag_awaits_approval'))
                    <div class="alert alert-success" role="alert">
                        <strong> <ion-icon name="checkmark-circle-outline"></ion-icon> {{Session::get('message')}} </strong> 
                    </div>
                @endif
            </div>
        </div>
    </div>        
    @endsection
@endif


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow">
                <h4 class="card-header text-center"> <strong>Transfer Point from {{$subject->title}}</strong></h4>

                <div class="card-body">


                    <div class="container">
                        
                        <div class="alert alert-primary" role="alert">
                        <ion-icon name="bulb-outline"></ion-icon> To reward someone with points, provide the his / her's <strong>TTa code</strong>, <strong>Subject</strong> and the <strong>Amount</strong> . Finally, write down the reasons for your Point Exchange.
                        </div>
                        <form method="POST" action="{{'student.transfer.submit'}}">

                            @csrf
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label for="inputCity">TTA Code</label>
                                    <input type="text" name="code" class="form-control {{ $errors->has('code') ? 'is-invalid' : ''}}" id="inputCity" placeholder="Example: gKphP" value="{{ old('code')}}">

                                    @if($errors->has('code'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('code')}}
                                    </div>
                                    @endif
                                </div>

                                <div class="form-group col-md-4">
                                    <label for="inputState">Subject</label>
                                    <select id="inputState" class="form-control  {{ $errors->has('subject') ? 'is-invalid' : ''}}" name="subject" value="{{ old('subject') }}">
                                        @foreach($subjects as $item)
                                        <option value="{{$item->id}}">{{$item->title}}</option>
                                        @endforeach
                                    </select>

                                    @if($errors->has('subject'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('subject')}}
                                    </div>
                                    @endif
                                </div>

                                <div>
                                    <input type="hidden" name="subject_id_from" id="" value="{{$subject->id}}">
                                </div>    

                                <div class="form-group col-md-4">
                                    <label for="inputCity">Amount</label>
                                    <input type="number" name="amount" min="1" max="{{auth()->user()->score($subject->id)}}" value="{{ old('amount') }}" class="form-control  {{ $errors->has('amount') ? 'is-invalid' : ''}}" id="" placeholder="Example: 50" title="Three letter country code">

                                    @if($errors->has('amount'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('amount')}}
                                    </div>
                                    @endif
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="explanation">Why are You transfering the Points?</label>
                                    <textarea class="form-control  {{ $errors->has('comment') ? 'is-invalid' : ''}}" name="comment" id="explanation" rows="3">{{ Session::has('comment_field_value') ? Session::get('comment_field_value') : '' }} {{ old('comment') }}</textarea>

                                    @if($errors->has('comment'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('comment')}}
                                    </div>
                                    @endif
                                </div>    
                            </div>

                            <button type="submit" class="btn btn-primary">Make Transfer</button>

                        </form>    

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
