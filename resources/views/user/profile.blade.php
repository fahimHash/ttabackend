@extends('layouts.app')


@if(Session::has('message'))
    @section('message')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                    <ion-icon name='checkmark-outline'></ion-icon>
                    <strong>{{ Session::get('message') }}</strong>
                </div>
            </div>
        </div>
    </div>
    @endsection
@endif

@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-3">
            <img src="{{ Auth::user()->photo }}" class="img-thumbnail shadow">

            <form class="mt-1" id="photoUploadForm" method="post"
                action="{{ route('student.profile.update.photo') }}" enctype="multipart/form-data">
                @csrf
                <div class="input-group">
                    <div class="custom-file">
                        <input type="file" name="photo" class="custom-file-input" id="profileImage" required>
                        <label class="custom-file-label" for="profileImage">
                            <ion-icon name="image-outline"></ion-icon>
                        </label>
                    </div>
                    <div class="input-group-append">
                        <input class="btn btn-primary" type="submit"></input>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-9">
            <div class="card">
                <h4 class="card-header"> {{ auth()->user()->name }} </h4>

                <div class="card-body">
                    <table class="table">

                        <tbody>

                            <tr>
                                <td><strong>Name</strong> </td>
                                <td>{{ auth()->user()->name }}</td>
                            </tr>

                            <tr>
                                <td><strong>email address</strong> </td>
                                <td>{{ auth()->user()->email }}</td>
                            </tr>

                            <tr>
                                <td><strong>TTA Code</strong> </td>
                                <td>{{ auth()->user()->tta_code }}</td>
                            </tr>

                            <tr>
                                <td><strong>Gender</strong> </td>
                                <td>{{ auth()->user()->gender }}</td>
                            </tr>

                            <tr>
                                <td><strong>Date of Birth</strong> </td>
                                <td>{{ Carbon\Carbon::parse(auth()->user()->dob)->toFormattedDateString() }}
                                </td>
                            </tr>

                        </tbody>
                    </table>

                    <a href="" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Edit Profile</a>
                </div>
            </div>

            <!-- <div class="card mt-4">
                <h4 class="card-header"> About Me </h4>
                <div class="card-body">
                    <div class="bio">
                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.
                    </div>


                </div>
            </div> -->

        </div>

        <!-- MODAL WINDOW -->

        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Update Profile</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <form action="{{ route('student.profile.update.bulk') }}" method="post"
                            id="profileUpdateForm">
                            @csrf


                            <div class="input-group mb-3 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">E-mail</div>
                                </div>
                                <input type="email" class="form-control" id="email" name="email"
                                    value="{{ auth()->user()->email }}" required>
                            </div>

                            <div class="input-group mb-3 mr-sm-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">Gender</div>
                                </div>
                                <select class="custom-select" id="inlineFormCustomSelect" name="gender" required>
                                    <option 
                                        @if( auth()->user()->gender != 'male' || auth()->user()->gender != 'female'  )
                                            selected
                                        @endif
                                    >Choose</option>

                                    <option value="male"
                                        @if( auth()->user()->gender == 'male' )
                                            selected
                                        @endif
                                    >Male</option>

                                    <option value="female"
                                        @if( auth()->user()->gender == 'female' )
                                            selected
                                        @endif
                                    >Female</option>
                                </select>
                            </div>

                            <div class="form-group">

                                <label><strong>Date of Birth</strong></label>

                                <input type="text" class="form-control" id="dob" name="dob"
                                    value="{{ auth()->user()->dob }}" required>
                            </div>

                            <div class="modal-footer">

                                <input class="btn btn-success btn-block" type="Submit" value="Update"></input>
                            </div>
                        </form>
                    </div>



                </div>
            </div>
        </div>

    </div>
</div>
@endsection


@section('scripts')

<script>
    $('#profileImage').on('change', function () {
        //get the file name
        var fileName = $(this).val();
        //replace the "Choose a file" label
        $('#photoUploadForm').append(
            "<p style='color:#fff; font-style:italic'><ion-icon name='checkmark-outline'></ion-icon> image uploaded! click Submit</p>"
        );
    });

    $('#dob').flatpickr({
        maxDate: "today"
    });

</script>

@endsection
