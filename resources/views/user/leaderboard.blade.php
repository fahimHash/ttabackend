@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow">
                <h4 class="card-header text-center"> <strong>{{ $subject->title }}</strong></h4>

                <div class="card-body">


                    <div class="container">
                        <div class="row align-items-center pt-5 pb-5">
                            <div class="col-md-4">
                                <img src="{{ Auth::user()->photo }}" class="img-thumbnail shadow">
                            </div>

                            <div class="col-md-8">
                                <h2>Hi, {{ Auth::user()->name }}</h2>
                                <h4>You Earned, <strong>{{ Auth::user()->score($subject->id) }}</strong> Points</h4>

                                @if($rank)
                                    <h4>Rank: <strong>{{ $rank + 1 }}</strong></h4>
                                @else
                                    <p class="text-dark bg-warning p-1">You need to score more than zero in order for
                                        you to be ranked</p>
                                @endif
                                <br>
                                <div style="margin-bottom: 10px;"><ion-icon name="bulb-outline"></ion-icon> <i>Click this button to transfer points to another Student</i></div>
                                <a href="{{route('student.transfer.show', $subject->id)}}" class="btn btn-success">Transfer</a>
                                <!-- {{session(['subject_id' => $subject->id])}} -->
                                @if($students->isNotEmpty())
                                    @if($students->first()->id == Auth::user()->id)
                                        <hr>

                                        <blockquote class="blockquote text-left">
                                            <h4 class="mb-0">Congratualtions
                                                <strong>{{ Auth::user()->name }}</strong>, for being the Top Scorer
                                                in {{ $subject->title }}</h4>
                                            <footer class="blockquote-footer">Shams</footer>
                                        </blockquote>
                                    @endif
                                @endif
                            </div>
                        </div>


                        @if($students->isEmpty())
                            <div class="row">
                                <div class="col-md-12">

                                    <blockquote class="blockquote">
                                        <h3 class="mb-0">Currently no one is in the Top list.</h3>
                                    </blockquote>
                                    <h5>So, get your spot in here!</h5>
                                </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="col-md-12">
                                    <h3>Toppers</h3> <hr>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Name</th>
                                                <th scope="col">Points</th>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            @foreach($students as $student)
                                            <tr>
                                                <th>{{$loop->index+1}}</th>
                                                <td>
                                                    <img src="{{ $student->photo }}" class="img-thumbnail rounded-circle shadow" style="height:50px">
                                                    <span>{{ $student->name }}</span>
                                                </td>
                                                <td><p>{{ $student->score($subject->id) }}</p></td>
                                            </tr>
                                            @endforeach
                                        </tbody>

                                    </table>
                                </div>
                                
                            </div>
                        @endif

                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
