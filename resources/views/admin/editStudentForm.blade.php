@extends('layouts.app')

@if(Session::has('message'))

    @section('message')

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-12">

                <div class="alert alert-success" role="alert">

                <ion-icon name="checkmark-circle-outline"></ion-icon> <strong>{{Session::get('student')->name}}</strong> has been Updated! 

                </div>

            </div>

        </div>

    </div>       

    @endsection

@endif

@if($errors->any())

    @section('message')

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-12">

                <div class="alert alert-danger" role="alert">

                    @foreach ($errors->all() as $error)

                        <div>{{ $error }}</div>

                    @endforeach

                </div>

            </div>

        </div>

    </div>

    @endsection 

@endif

@section('content')

<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-12">

            <div class="card shadow">

                <div class="card-header"> 

                    <p class="d-inline">Edit Student info</p>

                </div>

                <div class="card-body">

                    <form method="POST" action="{{route('admin.student.manage.update', $student->id)}}">

                        @csrf

                        <div>
                            <p class="bg-primary text-light p-2">Student Details</p>
                        </div>

                        <div class="form-row">

                            <div class="form-group col-md-4">

                                <label for="name">Full Name</label>

                                <input type="text" name="name" class="form-control form-control-sm" id="name" value="{{$student->name}}">

                            </div>

                            <div class="form-group col-md-8">

                                <label for="inputPassword4">Change Password</label>

                                <input type="text" name="password" class="form-control form-control-sm" id="inputPassword4" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">

                            </div>

                        </div>

                        <div class="form-group">

                            <label for="email">Email Address</label>

                            <input type="email" name="email" class="form-control form-control-sm" id="email" value="{{$student->email}}">

                        </div>

                        <div class="form-group">

                            <label for="email">Comment</label>

                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" cols="6" name="comment">{{ $student->comment }}</textarea>

                        </div>

                        <hr>    

                        <div>
                            <p class="bg-primary text-light p-2">Parent's Details</p>
                        </div>

                        <div class="form-row"> 

                            <div class="form-group col-md-4">

                                <label for="parent_name">Parent Name</label>

                                <input type="text" name="parent_name" class="form-control form-control-sm" id="parent_name" 
                                @if($student->guardian) value="{{ $student->guardian->name }}" @else value="" @endif>

                            </div>

                            <div class="form-group col-md-4">

                                <label for="parent_email">Parent email</label>

                                <input type="email" name="parent_email" class="form-control form-control-sm" id="parent_email" 
                                @if($student->guardian) value="{{ $student->guardian->email }}" @else value="" @endif>

                            </div>

                            <div class="form-group col-md-4">

                                <label for="parent_password">Parent Account Password</label>

                                <input type="text" name="parent_password" class="form-control form-control-sm" id="parent_password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">

                            </div>

                        </div>
                        
                        <hr>

                        <div>
                            <p class="bg-primary text-light p-2">Routine Details</p>
                        </div> 

                        <div class="form-group">

                            <table class="table table-bordered">

                                <thead>

                                    <tr>

                                        <th scope="col">Subjects</th>

                                        <th scope="col">Class Day / Time</th>

                                    </tr>

                                </thead>

                                <tbody>

                                    @foreach($subjects as $subject)

                                        <tr>
                                            
                                            <td>

                                                <div class="form-check">

                                                    @if($student->isEnrolledInSubject($subject->id))

                                                        <input class="form-check-input" type="checkbox" name="subjects[]" value="{{$subject->id}}" id="defaultCheck{{$subject->id}}" checked>

                                                        <label class="form-check-label" for="defaultCheck{{$subject->id}}">

                                                            {{$subject->title}}
                                                            
                                                        </label>

                                                    @else

                                                        <input class="form-check-input" type="checkbox" name="subjects[]" value="{{$subject->id}}" id="defaultCheck{{$subject->id}}">

                                                        <label class="form-check-label" for="defaultCheck{{$subject->id}}">

                                                            {{$subject->title}}

                                                        </label>

                                                    @endif

                                                </div>

                                            </td>

                                            <td>
                                        
                                                @foreach( $subject->routines as $routine )

                                                    <div class="form-check">

                                                        @if( $student->in_class( $routine->id ) )

                                                            <input class="form-check-input" type="checkbox" name="routines[]" value="{{$routine->id}}" id="defaultCheck{{$routine->id}}" checked>

                                                            <label class="form-check-label" for="defaultCheck{{$routine->id}}">

                                                                    {{$routine->day}} ({{$routine->start_time}} - {{$routine->end_time}})

                                                            </label>

                                                        @else

                                                            <input class="form-check-input" type="checkbox" name="routines[]" value="{{$routine->id}}" id="defaultCheck{{$routine->id}}" >

                                                            <label class="form-check-label" for="defaultCheck{{$routine->id}}">

                                                                    {{$routine->day}} ({{$routine->start_time}} - {{$routine->end_time}})

                                                            </label>    

                                                        @endif

                                                    </div>

                                                @endforeach

                                            </td>

                                        </tr>

                                    @endforeach

                                </tbody>

                            </table>

                        </div>
                        
                        <div class="form-group">

                            <label for="tta_code">TTA Code</label>

                            <input type="text" class="form-control" id="tta_code" value={{$student->tta_code}} readonly>

                        </div>

                        <button type="submit" class="btn btn-primary">Update</button>

                    </form>

                    <hr>

                    <form method="POST" action="{{route('admin.student.manage.delete', $student->id)}}">

                        @csrf

                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">Delete Student</button>

                    </form>

                </div>

            </div>

        </div>
        
    </div>

</div>

@endsection
