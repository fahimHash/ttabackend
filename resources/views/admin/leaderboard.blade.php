@extends('layouts.app')

@if(Session::has('response_message'))

@section('message')

<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-12">

            @if(Session::has('student'))

            <div class="alert alert-success" role="alert">

                <strong>

                    <ion-icon name="checkmark-circle-outline"></ion-icon> {{Session::get('student')->name}}'s
                    
                </strong> 
                
                score has been updated!

            </div>

            @else

            <div class="alert alert-warning" role="alert">

                <strong>

                    <ion-icon name="alert-circle-outline"></ion-icon> {{Session::get('response_message')}}

                </strong>

            </div>

            @endif

        </div>

    </div>

</div>

@endsection

@endif

@section('content')

<div class="container">

    <div class="row justify-content-center">


        <div class="col-md-12">

            <div class="card shadow">

                <h5 class="card-header"> 
                    
                    <div class="d-inline-block float-left"> {{$subject->title}} Leaderboard </div>   
                    
                    <div class="d-inline-block float-right">

                        <ion-icon name="cash-outline"></ion-icon>: {{ auth()->user()->admin_wallet }} points remaining in wallet 
                    
                    </div> 
                
                </h5>

                <div class="card-body">

                    <div class="container">

                        <div class="row justify-content-center">

                            @foreach($students as $student)

                            <div class="col-md-3">

                                <div class="card shadow-sm 

                                            <?php 

                                                if($loop->index < 3 AND $student->score($subject->id) > 0): 

                                                    echo ' leaderboard-top-student-card'; 

                                                endif; 

                                            ?>" style="display:inline-block;margin-bottom:30px">

                                    <img src="{{$student->photo}}" class="card-img-top">

                                    <div class="card-body" @if($loop->index == 0 AND $student->score($subject->id) > 0)

                                        style="background-color: #94ffc2;"

                                        @endif

                                        >

                                        <h5 class="card-title">

                                            {{$student->name}}

                                            @if($loop->index == 0 AND $student->score($subject->id) > 0)

                                            <ion-icon name="medal-outline"></ion-icon>

                                            @endif

                                        </h5>

                                        <h2 class="card-subtitle mb-2 text-muted" style="display:inline-block">

                                            {{$student->score($subject->id)}}

                                        </h2>

                                        @if( auth()->user()->can('update-point') ) 

                                            <form method="POST" action="{{route('admin.leaderboard.increase', [$subject->id, $student->id])}}">

                                                @csrf

                                                <div class="form-row">

                                                    <div class="form-group col-md-10">

                                                        <input type="number" class="form-control" id="increase" min="1" name="increment">

                                                    </div>

                                                    <div class="form-group col-md-2">

                                                        <button type="submit" class="card-link btn btn-success">+</button>

                                                    </div>
                                                    
                                                </div>

                                            </form>

                                            <form method="POST" action="{{route('admin.leaderboard.decrease', [$subject->id, $student->id])}}">

                                                @csrf

                                                <div class="form-row">

                                                    <div class="form-group col-md-10">

                                                        <input type="number" class="form-control" id="decrease" min="1" name="decrement">

                                                    </div>

                                                    <div class="form-group col-md-2">

                                                        <button type="submit" class="card-link btn btn-danger">-</button>

                                                    </div>

                                                </div>

                                            </form>
                                            
                                        @endif

                                    </div>

                                </div>

                            </div>

                            @endforeach

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection
