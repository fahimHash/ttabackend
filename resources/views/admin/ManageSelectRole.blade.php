@extends('layouts.app')

@if(Session::has('response_message'))
    @section('message')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                <ion-icon name="checkmark-circle-outline"></ion-icon> <strong>{{Session::get('response_message')}}</strong></div>
            </div>
        </div>
    </div>        
    @endsection
@endif

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header">Manage Role</div>

                <div class="card-body">
                    <form method="post" action="{{ route('admin.settings.role.manage') }}">

                        @csrf
                        <div class="form-row">

                            <div class="form-group col-md-12">
                                <label for="full_name">Select Role</label>

                                <select class="form-control" name="selected_role">
                                    @foreach( $roles as $role )

                                        @if( $role->name != 'super-admin' ) 
                                            <option value="{{ $role->id }}"> {{ $role->name }} </option>
                                        @endif

                                    @endforeach
                                </select>

                            </div>

                        </div>

                        <button type="submit" class="btn btn-primary mt-3">Proceed</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
