@extends('layouts.app')

@if(Session::has('response_message'))
    @section('message')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="alert alert-success" role="alert">
                    <strong>{{Session::get('response_message')}}</strong></div>
            </div>
        </div>
    </div>        
    @endsection
@endif


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header">Create new Admin</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.settings.admin.store') }}">

                        @csrf
                        <div class="form-row">

                            <div class="form-group col-md-6">
                                <label for="full_name">Name</label>
                                <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : ''}}" id="full_name" required value="{{ old('name')}}">

                                @if($errors->has('name'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('name')}}
                                    </div>
                                @endif

                            </div>

                            <div class="form-group col-md-6">
                                <label for="inputPassword4">Password</label>
                                <input type="text" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : ''}}" id="inputPassword4" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" value="{{ old('password')}}">

                                @if($errors->has('password'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('password')}}
                                    </div>
                                @endif

                            </div>

                        </div>

                        <div class="form-row">

                            <div class="form-group col-md-12">
                                <label for="email">Email Address</label>
                                <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : ''}}" id="email" required value="{{ old('email')}}">

                                @if($errors->has('email'))
                                    <div class="invalid-feedback">
                                        {{$errors->first('email')}}
                                    </div>
                                @endif

                            </div>

                        </div>

                        <!-- <div class="form-row">
                            <div class="form-group col-md-12">

                                <label for="email">Assign Permissions</label>
                                <hr>

                                @foreach($permissions as $permission)

                                <div class="form-check-inline m-1">

                                    <input class="form-check-input" type="checkbox" id="{{ $permission->name }}" value="{{ $permission->id }}" name="permissions[]">

                                    <label class="form-check-label" for="{{ $permission->name }}"> {{ ucwords($permission->name) }} </label>

                                </div>

                                @endforeach
                            </div>
                        </div> -->

                        <button type="submit" class="btn btn-primary mt-3">Create</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- <br>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header">Manage Admins</div>

                <div class="card-body">
                    <table class="table table-striped list-of-subjects">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Edit</th>
                            </tr>
                        </thead>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> -->
@endsection


@section('scripts')

<script>
    // $(document).ready( function () {
    //     $('.list-of-subjects').DataTable({
    //         processing: true,
    //         serverSide: true,
    //         ajax: "{!! route('admin.subject.manage.data') !!}",
    //         columns: [
    //             {data: 'id', name: 'id'},
    //             {data: 'title', name: 'title'},
    //             {data: 'action', name: 'action', orderable: false, searchable: false},
    //         ]
    //     });
    // } );
</script>

@endsection