@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header">Manage <strong>{{ $selected_role->name }}</strong>  Role</div>

                <div class="card-body">
                    <form method="post" action="{{ route('admin.settings.role.manage.update') }}">

                        @csrf
                        <div class="form-row">

                            <div class="form-group col-md-12">
                                <label> <strong>Select permissions for this Role</strong> </label>

                                <hr>
                                
                                @foreach( $permissions as $permission )

                                <div class="form-check form-check-inline">

                                    <input class="form-check-input" type="checkbox" id="{{ $permission->name }}" name="permissions[]" 
                                    value="{{$permission->id}}"
                                        @if($selected_role->permissions->contains($permission))
                                            checked
                                        @endif
                                    >
                                    <label class="form-check-label" for="{{ $permission->name }}"> {{ $permission->name }} </label>

                                </div>
                
                                @endforeach
                                
                                <input type="hidden" name="role" id="" value="{{ $selected_role->id }}">

                            </div>

                        </div>

                        <button type="submit" class="btn btn-primary mt-3">Update</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
