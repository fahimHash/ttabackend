@extends('layouts.app')

@if(Session::has('message'))

    @section('message')

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-12">

                <div class="alert alert-success" role="alert">

                    <ion-icon name="checkmark-circle-outline"></ion-icon> <strong>{{Session::get('student')->name}}</strong> has been added! 
                    <a class="float-right ml-3" href="">Edit</a>

                    <a class="float-right" href="">See Profile</a>

                </div>

            </div>

        </div>

    </div>  

    @endsection

@endif

@if($errors->any())

    @section('message')

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-8">

                <div class="alert alert-danger" role="alert">

                @foreach ($errors->all() as $error)

                    <div>{{ $error }}</div>

                @endforeach

                </div>

            </div>

        </div>

    </div>

    @endsection 

@endif

@section('content')

<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-12">

            <div class="card shadow">
            
                <div class="card-header">Add New Student</div>

                <div class="card-body">

                    <form method="POST" action="{{ route('admin.student.new') }}">

                        @csrf

                        <div>

                            <p class="bg-primary text-light p-2">Student Details</p>

                        </div>


                        <div class="form-row">

                            <div class="form-group col-md-6">

                                <label for="full_name">Full Name</label>

                                <input type="text" name="fullName" class="form-control form-control-sm" id="full_name" required>

                            </div>

                            <div class="form-group col-md-6">

                                <label for="inputPassword4">Password</label>

                                <input type="text" name="password" class="form-control form-control-sm" id="inputPassword4" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" required title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">
                                
                            </div>

                        </div>

                        <div class="form-group">

                            <label for="email">Email Address</label>

                            <input type="email" name="email" class="form-control form-control-sm" id="email" required>

                        </div>

                        <hr>    

                        <div>
                            <p class="bg-primary text-light p-2">Parent's Details</p>
                        </div>

                        <div class="form-row"> 

                            <div class="form-group col-md-4">

                                <label for="parent_name">Parent Name</label>

                                <input type="text" name="parent_name" class="form-control form-control-sm" id="parent_name" value="">

                            </div>

                            <div class="form-group col-md-4">

                                <label for="parent_email">Parent email</label>

                                <input type="email" name="parent_email" class="form-control form-control-sm" id="parent_email" value="" >

                            </div>

                            <div class="form-group col-md-4">

                                <label for="parent_password">Parent Account Password</label>

                                <input type="text" name="parent_password" class="form-control form-control-sm" id="parent_password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters">

                            </div>

                        </div>

                        <hr>

                        <div>
                            <p class="bg-primary text-light p-2">Routine Details</p>
                        </div> 

                        <div class="form-group">

                            <label for="">Prepare Routine</label>

                            <table class="table table-bordered">

                                <thead>

                                    <tr>

                                        <th scope="col">Subjects</th>

                                        <th scope="col">Class Day / Time</th>

                                    </tr>

                                </thead>

                                <tbody>

                                    @foreach($subjects as $subject)

                                        <tr>
                                            
                                            <td>

                                                <div class="form-check">

                                                    <input class="form-check-input" type="checkbox" name="subjects[]" value="{{$subject->id}}" id="defaultCheck{{$subject->id}}">

                                                    <label class="form-check-label" for="defaultCheck{{$subject->id}}">

                                                        {{$subject->title}}

                                                    </label>

                                                </div>

                                            </td>

                                            <td>
                                        
                                                @foreach( $subject->routines as $routine )

                                                    <div class="form-check">

                                                        <input class="form-check-input" type="checkbox" name="routines[]" value="{{$routine->id}}" id="defaultCheck{{$routine->id}}" >

                                                        <label class="form-check-label" for="defaultCheck{{$routine->id}}">

                                                               ({{ $routine->section->name }}) - {{$routine->day}} ({{$routine->start_time}} - {{$routine->end_time}})

                                                        </label>    

                                                    </div>

                                                @endforeach

                                            </td>

                                        </tr>

                                    @endforeach

                                </tbody>

                            </table>

                        </div>
                        

                        <div class="form-group">

                            <label for="tta_code">TTA Code</label>

                            <input type="text" name="ttaCode" class="form-control" id="tta_code" value={{str_random(5)}} required readonly>

                        </div>

                        <button type="submit" class="btn btn-primary">Add</button>

                    </form>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection
