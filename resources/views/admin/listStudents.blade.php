@extends('layouts.app')

@if(Session::has('remove_notice'))

    @section('message')

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-12">

                <div class="alert alert-success" role="alert">

                    <ion-icon name="checkmark-circle-outline"></ion-icon> <strong>{{ Session::get('remove_notice') }}</strong>

                </div>

            </div>

        </div>

    </div>     

    @endsection

@endif


@if($errors->any())

    @section('message')

    <div class="container">

        <div class="row justify-content-center">

            <div class="col-md-12">

                <div class="alert alert-danger" role="alert">
                    
                </div>

            </div>

        </div>

    </div>

    @endsection 

@endif

@section('content')

<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-12">

            <div class="card shadow">

                <div class="card-header">Manage Students</div>

                <div class="card-body">

                    <table class="table table-striped list-of-students">

                        <thead>

                            <tr>

                                <th scope="col">#</th>

                                <th scope="col">Name</th>

                                <th scope="col">Email</th>

                                <th scope="col">Subjects</th>

                                <th scope="col">TTA Code</th>

                                <th scope="col">Edit</th>

                            </tr>

                        </thead>
                        
                    </table>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection


@section('scripts')

<script>

    $(document).ready( function () {
        
        $('.list-of-students').DataTable({

            processing: true,

            serverSide: true,

            ajax: "{!! route('admin.students.manage.data') !!}",

            columns: [

                {data: 'id', name: 'id'},

                {data: 'name', name: 'name'},

                {data: 'email', name: 'email'},

                {data: 'subjects[, ].title', name: 'subjects.title', orderable: false},

                {data: 'tta_code', name: 'tta_code', orderable: false},

                {data: 'action', name: 'action', orderable: false, searchable: false},

            ]

        });

    } );

</script>

@endsection