@extends('layouts.app')

@if(Session::has('message'))
@section('message')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="alert alert-success" role="alert">
            <ion-icon name="checkmark-circle-outline"></ion-icon> {{ Session::get('message') }}
            </div>
        </div>
    </div>
</div>
@endsection
@endif


@section('content')
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-4">
            <div class="card shadow">
                <div class="card-header">Points Remaining in Your wallet</div>

                <div class="card-body">
                    <p> <ion-icon name="cash-outline"></ion-icon>:  <strong>{{auth()->user()->admin_wallet}}</strong> Points</p>
                </div>
            </div>
        </div>


        <div class="col-md-8">
            <div class="card shadow">
                <div class="card-header">Generate Point</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.point.generate') }}">

                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="title">Amount</label>
                                <input type="number" name="amount" class="form-control" id="amount" required placeholder="e.g. 5000" min="100">
                            </div>

                            <div class="form-group col-md-6">
                                <label for="title">To</label>

                                <select class="form-control" name="allocate_to">
                                    @foreach( $admins as $admin )
                                        <option value="{{ $admin->id }}"> {{ $admin->name }} </option>
                                    @endforeach
                                </select>

                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Generate</button>

                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
