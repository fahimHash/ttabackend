@extends('layouts.app')

@if(Session::has('message'))
    @section('message')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="alert alert-success" role="alert">
                    <strong>{{Session::get('subject')->title}}</strong> has been added!
                </div>
            </div>
        </div>
    </div>        
    @endsection
@endif

@if(Session::has('delete_notice'))
    @section('message')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="alert alert-success" role="alert">
                    <strong>{{Session::get('delete_notice')}}</strong>
                </div>
            </div>
        </div>
    </div>        
    @endsection
@endif

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header">Add New Subject</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.subject.new') }}">

                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="title">Subject Title</label>
                                <input type="text" name="title" class="form-control" id="title" required>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Add</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header">Manage Subjects</div>

                <div class="card-body">
                    <table class="table table-striped list-of-subjects">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Title</th>
                            <th scope="col">Edit</th>
                            </tr>
                        </thead>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')

<script>
    $(document).ready( function () {
        $('.list-of-subjects').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{!! route('admin.subject.manage.data') !!}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'title', name: 'title'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
    } );
</script>

@endsection