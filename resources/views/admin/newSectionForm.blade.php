@extends('layouts.app')

@if(Session::has('response_message'))

    @section('message')

        <div class="container">

            <div class="row justify-content-center">

                <div class="col-md-12">

                    <div class="alert alert-success" role="alert">

                    <ion-icon name="checkmark-circle-outline"></ion-icon> <strong>{{Session::get('response_message')}}</strong></div>

                </div>


            </div>

        </div>      

    @endsection

@endif

@section('content')

<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-12">

            <div class="card shadow">

                <div class="card-header">Create New Section</div>

                <div class="card-body">

                    <form method="post" action="{{ route('admin.section.store') }}">

                        @csrf

                        <div class="form-row">

                            <div class="form-group col-md-6">

                                <label for="name">Section Name</label>

                                <input type="text" name="name" class="form-control" id="name" required>

                            </div>

                            <div class="form-group col-md-6">

                                <label for="subjects">Subject ?</label>

                                <select class="form-control" id="subjects" name="subject" required >

                                    @foreach( $subjects as $subject )    

                                        <option value="{{ $subject->id }}"> {{ $subject->title }} </option>

                                    @endforeach

                                </select>

                            </div>

                        </div>

                        <!-- <h6><b>Make a Routine</b> <i>(day 2 is required if a section has classes twice a week)</i></h6> -->

                        <hr>

                        <div class="form-row">
                        
                            <div class="form-group col-md-3">

                                <label for="day">Day 1</label>

                                <select class="form-control" id="day" name="day" required >

                                    <option value="saturday"> Saturday </option>

                                    <option value="sunday"> Sunday </option>

                                    <option value="monday"> Monday </option>

                                    <option value="tuesday"> Tuesday </option>

                                    <option value="wednesday"> Wednesday </option>

                                    <option value="Thursday"> Thursday </option>

                                </select>

                            </div>

                            <div class="form-group col-md-3">

                                <label for="start_time">Class Start Time?</label>

                                <input type="text" name="start_time" class="form-control" id="start_time" required readonly="readonly">

                            </div>

                            <div class="form-group col-md-3">

                                <label for="end_time">Class End Time?</label>

                                <input type="text" name="end_time" class="form-control" id="end_time" required readonly="readonly">

                            </div>

                            <div class="form-group col-md-3">

                                <label for="admins">Assign Teacher</label>

                                <select class="form-control" id="admins" name="teacher" required >

                                    @foreach( $admins as $admin )    

                                        <option value="{{ $admin->id }}"> {{ $admin->name }} </option>

                                    @endforeach

                                </select>

                            </div>

                        </div>

                        <div class="form-row day_2_routine">

                        </div>
                        
                        <hr>

                        <div class="button-group extra_day">

                            <button class="btn btn btn-secondary add_another_day">

                                <ion-icon name="add-circle-outline"></ion-icon> Add Day 2

                            </button>

                            <button class="btn btn btn-danger remove_another_day">

                                <ion-icon name="close-circle-outline"></ion-icon> Remove Day 2

                            </button>

                        </div>

                        <hr>

                        <button type="submit" class="btn btn-primary">Create</button>

                    </form>

                </div>

            </div>

        </div>

    </div>

    <br>

    <div class="row justify-content-center">

        <div class="col-md-12">

            <div class="card shadow">

                <div class="card-header">Manage Sections</div>


                <div class="card-body">

                    <table class="table table-striped list-of-sections">

                        <thead>

                            <tr>

                                <th scope="col">Name</th>

                                <th scope="col">Subject</th>

                                <th scope="col">Edit</th>

                            </tr>

                        </thead>
                        
                    </table>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection


@section('scripts')

<script>
    
    $('#start_time').flatpickr({

        enableTime: true,

        noCalendar: true,

        time_24hr: true

    });

    $('#end_time').flatpickr({

        enableTime: true,

        noCalendar: true,

        time_24hr: true

    });

    $('.remove_another_day').hide();

    var button = document.querySelector('.add_another_day').addEventListener("click", function(e) {

        e.preventDefault();

        $(this).hide();

        $('.remove_another_day').show();

        document.querySelector('.day_2_routine').innerHTML = 
        
        `
            <div class="form-group col-md-3">

                <label for="day2">Day 2 </label>

                <select class="form-control" id="day2" name="day_2" >

                    <option value="saturday"> Saturday </option>

                    <option value="sunday"> Sunday </option>

                    <option value="monday"> Monday </option>

                    <option value="tuesday"> Tuesday </option>

                    <option value="wednesday"> Wednesday </option>

                    <option value="Thursday"> Thursday </option>

                </select>

            </div>

            <div class="form-group col-md-3">

                <label for="start_time_2">Class Start Time? </label>

                <input type="text" name="start_time_2" class="form-control" id="start_time_2" readonly="readonly">

            </div>

            <div class="form-group col-md-3">

                <label for="end_time_2">Class End Time?</label>

                <input type="text" name="end_time_2" class="form-control" id="end_time_2" readonly="readonly">

            </div>

            <div class="form-group col-md-3">

                <label for="admins">Assign Teacher</label>

                <select class="form-control" id="admins" name="teacher_2" required >

                    @foreach( $admins as $admin )    

                        <option value="{{ $admin->id }}"> {{ $admin->name }} </option>

                    @endforeach

                </select>

            </div>
        
        `;

        $('#start_time_2').flatpickr({

            enableTime: true,

            noCalendar: true,

            time_24hr: true

        });

        $('#end_time_2').flatpickr({

            enableTime: true,

            noCalendar: true,

            time_24hr: true

        });

    });


    $(document).ready( function () {

        $('.list-of-sections').DataTable({

            processing: true,

            serverSide: true,

            ajax: "{!! route('admin.section.manage.ajax') !!}",

            columns: [

                {data: 'name', name: 'name'},

                {data: 'subject', name: 'subject'},

                {data: 'action', name: 'action', orderable: false, searchable: false},

            ]

        });

        $('.remove_another_day').click( function( e ){

            e.preventDefault();

            $('.add_another_day').show();

            $('.day_2_routine').empty();

            $(this).hide();

        });

    } );
    
</script>

@endsection