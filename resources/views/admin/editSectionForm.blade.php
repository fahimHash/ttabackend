@extends('layouts.app')

@if(Session::has('response_message'))

    @section('message')

        <div class="container">

            <div class="row justify-content-center">

                <div class="col-md-12">

                    <div class="alert alert-success" role="alert">

                    <ion-icon name="checkmark-circle-outline"></ion-icon> <strong>{{Session::get('response_message')}}</strong></div>

                </div>


            </div>

        </div>      

    @endsection

@endif

@section('content')

<div class="container">

    <div class="row justify-content-center">

        <div class="col-md-12">

            <div class="card shadow">

                <div class="card-header">
                   
                   <a class="btn btn-sm btn-primary" href="{{ url()->previous() }}" style="margin-right:10px;"> < Go Back</a> |
                   
                   <span style="display:inline-block;margin-left:10px;">Edit Section</span>

                </div>

                <div class="card-body">

                    <form method="post" action="{{ route( 'admin.section.manage.update', $section->id ) }}">

                        @csrf

                        <div class="form-row">

                            <div class="form-group col-md-6">

                                <label for="name">Section Name</label>

                                <input type="text" name="name" class="form-control" id="name" required value="{{ $section->name }}">

                            </div>

                            <div class="form-group col-md-6">

                                <label for="subjects">Subject ?</label>

                                <select class="form-control" id="subjects" name="subject" required >

                                    @foreach( $subjects as $subject )    

                                        @if( $subject->id == $section->subject->id )

                                            <option value="{{ $subject->id }}" selected> {{ $subject->title }} </option>

                                        @else    

                                            <option value="{{ $subject->id }}"> {{ $subject->title }} </option>

                                        @endif

                                    @endforeach

                                </select>

                            </div>

                        </div>

                        <hr>

                        <?php $no_of_sections = 0; ?>

                        @foreach( $section->routines as $index => $routine )

                            <div class="form-row">
                            
                                <div class="form-group col-md-3">

                                    <label for="day">Day {{ $index + 1 }}</label>

                                    <select class="form-control" id="day" name="day_{{$routine->id}}" required >

                                        <option value="saturday" {{ $routine->day === 'saturday' ? 'selected' : '' }} > Saturday </option>

                                        <option value="sunday" {{ $routine->day === 'sunday' ? 'selected' : '' }} > Sunday </option>

                                        <option value="monday" {{ $routine->day === 'monday' ? 'selected' : '' }} > Monday </option>

                                        <option value="tuesday" {{ $routine->day === 'tuesday' ? 'selected' : '' }} > Tuesday </option>

                                        <option value="wednesday" {{ $routine->day === 'wednesday' ? 'selected' : '' }} > Wednesday </option>

                                        <option value="thursday" {{ $routine->day === 'thursday' ? 'selected' : '' }} > Thursday </option>

                                    </select>

                                </div>

                                <div class="form-group col-md-3">

                                    <label for="start_time">Class Start Time?</label>

                                    <input type="text" name="start_time_{{$routine->id}}" class="form-control start_time" required readonly="readonly" 
                                    
                                    value={{ $routine->start_time }}>

                                </div>

                                <div class="form-group col-md-3">

                                    <label for="end_time">Class End Time?</label>

                                    <input type="text" name="end_time_{{$routine->id}}" class="form-control end_time" required readonly="readonly"
                                    
                                    value={{ $routine->end_time }}>

                                </div>

                                <div class="form-group col-md-3">

                                    <label for="admins">Assign Teacher</label>

                                    <select class="form-control" id="admins" name="teacher_{{$routine->id}}" required >

                                        @foreach( $admins as $admin )    

                                            @if( $routine->admin_id == $admin->id )
                                                
                                                <option value="{{ $admin->id }}" selected> {{ $admin->name }} </option>
                                            
                                            @else
                                                
                                                <option value="{{ $admin->id }}"> {{ $admin->name }} </option>
                                            
                                            @endif

                                        @endforeach

                                    </select>

                                </div>

                                <input type="hidden" name="routine_id[]" id="" value="{{ $routine->id }}">

                            </div>

                            <?php $no_of_sections = $no_of_sections + 1; ?>

                        @endforeach
                        
                        @if( $no_of_sections < 2 )

                            <div class="form-row day_2_routine">

                            </div>

                            <hr>

                            <div class="button-group extra_day">

                                <button class="btn btn btn-secondary add_another_day">

                                    <ion-icon name="add-circle-outline"></ion-icon> Add Day 2

                                </button>

                                <button class="btn btn btn-danger remove_another_day">

                                    <ion-icon name="close-circle-outline"></ion-icon></ion-icon> Remove Day 2

                                </button>

                            </div>

                        @endif

                        <hr>

                        <div class="mt-2 text-right">

                            <button type="submit" class="btn btn-success">Update</button>
                        
                        </div>

                    </form>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection


@section('scripts')

<script>
    
    $('.start_time').flatpickr({

        enableTime: true,

        noCalendar: true,

        time_24hr: true

    });

    $('.end_time').flatpickr({

        enableTime: true,

        noCalendar: true,

        time_24hr: true

    });

    $('.remove_another_day').hide();

    @if( $no_of_sections < 2 )

        var button = document.querySelector('.add_another_day').addEventListener("click", function(e) {

            e.preventDefault();

            $(this).hide();

            $('.remove_another_day').show();

            document.querySelector('.day_2_routine').innerHTML = 
            
            `
                <div class="form-group col-md-3">

                    <label for="day2">Day 2 </label>

                    <select class="form-control" id="day2" name="day_2" >

                        <option value="saturday"> Saturday </option>

                        <option value="sunday"> Sunday </option>

                        <option value="monday"> Monday </option>

                        <option value="tuesday"> Tuesday </option>

                        <option value="wednesday"> Wednesday </option>

                        <option value="Thursday"> Thursday </option>

                    </select>

                </div>

                <div class="form-group col-md-3">

                    <label for="start_time_2">Class Start Time? </label>

                    <input type="text" name="start_time_2" class="form-control start_time_2" readonly="readonly">

                </div>

                <div class="form-group col-md-3">

                    <label for="end_time_2">Class End Time?</label>

                    <input type="text" name="end_time_2" class="form-control end_time_2" readonly="readonly">

                </div>

                <div class="form-group col-md-3">

                    <label for="admins">Assign Teacher</label>

                    <select class="form-control" id="admins" name="teacher_2" required >

                        @foreach( $admins as $admin )    

                            <option value="{{ $admin->id }}"> {{ $admin->name }} </option>

                        @endforeach

                    </select>

                </div>
            
            `;

            $('.start_time_2').flatpickr({

                enableTime: true,

                noCalendar: true,

                time_24hr: true

            });

            $('.end_time_2').flatpickr({

                enableTime: true,

                noCalendar: true,

                time_24hr: true

            });

        });


        $(document).ready( function () {

            $('.remove_another_day').click( function( e ){

                e.preventDefault();

                $('.add_another_day').show();

                $('.day_2_routine').empty();

                $(this).hide();

            });

        } );

    @endif
    
</script>

@endsection