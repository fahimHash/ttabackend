@extends('layouts.app')

@if(Session::has('message'))
    @section('message')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="alert alert-success" role="alert">
                    <strong>{{Session::get('subject')->title}}</strong> has been Updated!
                </div>
            </div>
        </div>
    </div>        
    @endsection
@endif

@if(Session::has('invalid_notice'))
    @section('message')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="alert alert-warning" role="alert">
                    <strong>{{Session::get('invalid_notice')}}</strong>
                </div>
            </div>
        </div>
    </div>        
    @endsection
@endif

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header">Edit Subject</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin.subject.manage.update', $subject->id) }}">

                        @csrf
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="title">Subject Title</label>
                                <input type="text" name="title" class="form-control" id="title" value="{{$subject->title}}">
                            </div>
                        </div>

                        <button type="submit" class="btn btn-primary">Update</button>

                    </form>

                    <hr>

                    <form method="POST" action="{{route('admin.subject.manage.delete', $subject->id)}}">
                        @csrf
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure you want to delete this item?');">Delete Subject</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
