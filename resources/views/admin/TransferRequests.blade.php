@extends('layouts.app')


@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow">
                <div class="card-header text-center"> <h5>Point Transfer Requests</h5> </div>

                <div class="card-body">
                    @if($collection->isEmpty())

                    <div class="alert alert-success text-center" role="alert">
                        Currently there are no pending Point Transfer requests
                    </div>

                    @else
                    <table class="table table-bordered table-hover table-sm">
                        <thead class="thead-dark">
                            <tr class="d-flex">
                                <th scope="col" class="col-1">#</th>
                                <th scope="col" class="col-2">From</th>
                                <th scope="col" class="col-2">To</th>
                                <th scope="col" class="col-1 text-center">Points</th>
                                <th scope="col" class="col-3">Reason</th>
                                <th scope="col" class="col-1 text-center">Date</th>
                                <th scope="col" class="col-1 text-center">Approve</th>
                                <th scope="col" class="col-1 text-center">Reject</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($collection as $index => $request)
                            
                            <tr class="d-flex">

                                <td scope="row" class="col-1"> {{ $index + 1 }} </td>
                                            
                                <td scope="row" class="col-2"> <strong>{{ $request['from']->name }}</strong>, <i> {{ $request['subject_from']->title }} </i> </td>
                  
                                <td scope="row" class="col-2">  <strong>{{ $request['to']->name }}</strong>, <i> {{ $request['subject_to']->title }} </i> </td>
                    
                                <td scope="row" class="col-1 text-center"> {{ $request['amount'] }} </td>
             
                                <td scope="row" class="col-3"> {{ $request['comment'] }} </td>

                                <td scope="row" class="col-1"> {{ $request['date']->format('d-M-y') }} </td>

                                <td scope="row" class="col-1 text-center"> <a href="{{ route('admin.point.transfer.approve', $request['transfer_id']) }}" class="btn btn-sm btn-success">Accept</ion-icon></a> </td>

                                <td scope="row" class="col-1 text-center"> <a href="{{ route('admin.point.transfer.reject', $request['transfer_id']) }}" class="btn btn-sm btn-danger">Reject</ion-icon></a> </td>

                            </tr>

                            @endforeach
                        </tbody>   
                    </table>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
