@extends('layouts.app')

@section('content')

    <div class="container">

        <div class="row">

            <div class="col-md-12  mb-4">

                <div class="p-3" style="background-color:#fff">

                    <h5><ion-icon name="bulb-outline"></ion-icon> What can you do in this Dashboard</h5>
                    <hr>
                    <p>
                        Here you can see you kid's performance development. We are working on creating a model which will visualize your son/daughter's overall performance in our classes. So, you can keep track on his/her progress.
                    </p>

                    <p>
                        Furthermore, you can pay the monthly fees through the payment tab above ( <i>we are working on it, it'll be availavle soon</i> ). 
                    </p>

                    <p class="bg-info p-2">We are currently working on multiple functionalities, so that you can get more accurate overview of what we are doing in TTA.</p>

                </div>
                
            </div>

            @foreach( auth()->user()->users as $student )

            <div class="col-md-3">

                <div class="card">

                    <div class="card-header">

                        <img src="{{$student->photo}}" class="card-img-top" alt="...">

                    </div>

                    <div class="card-body">

                        <h5>{{$student->name}}</h5>

                        <hr>

                        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#Modal{{$student->id}}">
                            See Performance
                        </button>

                    </div>

                </div>        

            </div>

            <div class="modal fade" id="Modal{{$student->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">

                <div class="modal-dialog">

                    <div class="modal-content">

                        <div class="modal-header">

                            <h5 class="modal-title" id="exampleModalLabel">{{$student->name}}</h5>

                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">

                            <span aria-hidden="true">&times;</span>

                            </button>

                        </div>

                        <div class="modal-body">

                            <p>{{$student->comment}}</p>

                        </div>

                        <div class="modal-footer">

                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

                        </div>

                    </div>

                </div>

            </div>

            @endforeach   

        </div>

    </div>
@endsection
