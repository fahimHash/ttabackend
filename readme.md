## About Project

This is a portal for [The Tech Academy](https://thetechacademy.net/) students. The admins of this system engages with a 'virtual economy' that generates currency, allows then to allocate that and distribute among students as rewards or penalty. 

## Features

- Virtual economy.
- Multilevel admin.
- Parents review 
- Student attendence/fees management.
- Student project managemnet.
- Class/Batches monitoring.



## License

This project in under the owenership of [The Tech Academy Bangladesh](https://thetechacademy.net/).
