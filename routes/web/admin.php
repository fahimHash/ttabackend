<?php

Route::prefix('/admin')->group(function () {

    Route::get('/login', 'Admin\Auth\LoginController@showLoginForm')->name('admin.login');

    Route::post('/login', 'Admin\Auth\LoginController@login')->name('admin.login.submit');

    Route::post('/logout', 'Admin\Auth\LoginController@logout')->name('admin.logout');

    Route::get('/dashboard', 'Admin\AdminController@showDashboard')->name('admin.dashboard');

    // 
    
    Route::get('/student/new', 'Admin\ManageStudentsController@newStudentForm')->name('admin.student.new');

    Route::post('/student/new', 'Admin\ManageStudentsController@createNewStudent');

    Route::get('/student/manage/edit/{id}', 'Admin\ManageStudentsController@showEditForm')->name('admin.student.manage.edit');

    Route::post('/student/manage/update/{id}', 'Admin\ManageStudentsController@update')->name('admin.student.manage.update');

    Route::post('/student/manage/delete/{id}', 'Admin\ManageStudentsController@delete')->name('admin.student.manage.delete');

    Route::get('/students/manage', 'Admin\ManageStudentsController@manageStudents')->name('admin.students.manage');

    Route::get('/students/manage/ajax', 'Admin\ManageStudentsController@ajax')->name('admin.students.manage.data');

    //

    Route::get('/leaderboard/subject/{id}', 'Admin\LeaderBoardController@showLeaderBoard_of_a_Subject')->name('admin.leaderboard.subject.show');

    Route::post('/leaderboard/subject/{subject_id}/student/{student_id}/increase/',

        'Admin\LeaderBoardController@increaseScore')->name('admin.leaderboard.increase');

    Route::post('/leaderboard/subject/{subject_id}/student/{student_id}/decrease/',

        'Admin\LeaderBoardController@decreaseScore')->name('admin.leaderboard.decrease');

    Route::get('/subject/new', 'Subject\SubjectController@newSubjectForm')->name('admin.subject.new');

    Route::post('/subject/new', 'Subject\SubjectController@createNewSubject');

    Route::get('/subject/manage/ajax', 'Subject\SubjectController@ajax')->name('admin.subject.manage.data');

    Route::get('/subject/manage/edit/{id}', 'Subject\SubjectController@showEditForm')->name('admin.subject.manage.edit');

    Route::post('/subject/manage/edit/{id}', 'Subject\SubjectController@update')->name('admin.subject.manage.update');

    Route::post('/subject/manage/delete/{id}', 'Subject\SubjectController@delete')->name('admin.subject.manage.delete');

    // 

    Route::get('/point/generate/new', 'Admin\PointController@show_point_generation_page')->name('admin.point.newform');

    Route::post('/point/generate/new', 'Admin\PointController@generate_point')->name('admin.point.generate');

    Route::get('/point/transfer/requests', 'Admin\TransferController@pointTransferRequests')->name('admin.point.transfer.index');

    Route::get('/point/transfer/approve/{transfer}', 'Admin\TransferController@approveTransfer')->name('admin.point.transfer.approve');

    Route::get('/point/transfer/reject/{transfer}', 'Admin\TransferController@rejectTransfer')->name('admin.point.transfer.reject');

    //

    Route::get('/settings/admin/manage', 'Admin\AdminController@showManagePage')->name('admin.settings.admin.manage');

    Route::post('/settings/admin/store', 'Admin\AdminController@createAdmin')->name('admin.settings.admin.store');
    
    //

    Route::get('/settings/role/manage/select', 'Admin\AdminController@show_manage_select_role_page')->name('admin.settings.role.manage.select');

    Route::post('/settings/role/manage/', 'Admin\AdminController@show_manage_role_page')->name('admin.settings.role.manage');

    Route::post('/settings/role/manage/update', 'Admin\AdminController@update_role')->name('admin.settings.role.manage.update');

    //

    Route::get('/section/new', 'Subject\SubjectController@new_section_form')->name('admin.section.newform');

    Route::post('/section/new', 'Subject\SubjectController@store_section')->name('admin.section.store');

    Route::get('/section/manage/edit/{id}', 'Subject\SubjectController@show_section_edit_form')->name('admin.section.manage.edit');

    Route::post('/section/manage/edit/{id}', 'Subject\SubjectController@section_update_with_routine')->name('admin.section.manage.update');

    Route::get('/section/manage/ajax', 'Subject\SubjectController@section_ajax')->name('admin.section.manage.ajax');

});