<?php

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();