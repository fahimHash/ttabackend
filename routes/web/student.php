<?php

Route::prefix('/student')->group(function () {

    // Route::get('/{id}/leaderboard', 'User\LeaderBoardController@showLeaderBoard')->name('student.leaderboard.show');
    Route::get('/leaderboard/{subject_id}', 'User\LeaderBoardController@showLeaderBoard_of_a_Subject')->name('student.leaderboard.subject.show');
    Route::get('/profile', 'User\ProfileController@showProfile')->name('student.profile');
    Route::post('/profile/update/photo', 'User\ProfileController@updatePhoto')->name('student.profile.update.photo');
    Route::post('/profile/update/bulk', 'User\ProfileController@updateBulk')->name('student.profile.update.bulk');

    Route::get('/transfer/{subject_id}', 'User\TransferController@showTransferPage')->name('student.transfer.show');
    Route::post('/transfer/{subject_id}', 'User\TransferController@requestTransfer')->name('student.transfer.submit');
});