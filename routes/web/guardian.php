<?php

Route::prefix('/parent')->group(function () {

    Route::get('/login', 'Guardian\Auth\LoginController@showLoginForm')->name('guardian.login');

    Route::post('/login', 'Guardian\Auth\LoginController@login')->name('guardian.login.submit');

    Route::post('/logout', 'Guardian\Auth\LoginController@logout')->name('guardian.logout');

    Route::get('/dashboard', 'Guardian\GuardianController@showDashboard')->name('guardian.dashboard');

});