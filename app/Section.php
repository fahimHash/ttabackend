<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{

    public $timestamps = false;

    // protected $fillable = [ 'name' ];

    public function subject()
    {

        return $this->belongsTo('App\Subject');

    }

    public function routines()
    {

        return $this->hasMany('App\Routine');

    }
}
