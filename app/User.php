<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'dob', 'gender', 'bio', 'comment'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $guarded = [];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function guardian(){

        return $this->belongsTo('App\Guardian');

    }

    public function subjects(){

        return $this->belongsToMany('App\Subject')->withPivot('score');

    }

    public function routines()
    {
        return $this->belongsToMany('App\Routine');
    }

    public function isEnrolledInSubject($subject_id)
    {
        $result = $this->subjects->contains(Subject::find($subject_id));
        return $result;
    }

    public function in_class($routine_id)
    {
        $result = $this->routines->contains(Routine::find($routine_id));
        return $result;
    }
    

    public function score($subject_id)
    {
        return $this->subject($subject_id)->pivot->score;
    }
    
    // SCOPED FUNCTIONS
    
    public function scopeSubject($query, $subject_id)
    {   
        return $this->subjects->firstWhere('id', $subject_id);
    }

}
