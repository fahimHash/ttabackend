<?php

namespace App\Services\Transfer;

use App\Transfer;

class TransferService
{

    public function all()
    {
        return Transfer::all();
    }

    public function allPending()
    {
        return Transfer::where('approved', '=', false)->get();
    }

    public function create($data)
    {
        $transfer = new Transfer();
        $transfer->to = $data['to'];
        $transfer->from = $data['from'];
        $transfer->amount = $data['amount'];
        $transfer->subject_id_from = $data['subject_id_from'];
        $transfer->subject_id_to = $data['subject_id_to'];
        $transfer->comment = $data['comment'];
        $transfer->save();

        return $transfer;
    }

    public function getPendingTransfersByStudentAndSubject($student, $subject)
    {
        return Transfer::where('approved', '=', false)
                        ->where('from', '=', $student->id)
                        ->where('subject_id_from', '=', $subject->id)
                        ->get();
    }

    public function getPendingTransferAmountByStudentAndSubject($student, $subject)
    {
        $transfers = $this->getPendingTransfersByStudentAndSubject($student, $subject);

        return $transfers->sum('amount');
    }
    
    public function canThisStudentTransferPoint($student, $subject, $requested_amount)
    {
        if( $student->score($subject->id) - ( $this->getPendingTransferAmountByStudentAndSubject( $student, $subject ) + $requested_amount ) < 0 )
        {
            return false;
        }

        return true;
    }

}
