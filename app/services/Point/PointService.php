<?php

namespace App\Services\Point;

use App\Point;

class PointService
{

    public function create($amount, $allocating_by, $allocated_to)
    {   
        $point = new Point();
        $point->amount = $amount;
        $point->admin()->associate( $allocating_by );
        $point->allocated_to = $allocated_to->id;
        $point->save();
        return $point;
    }

    public function total_points_allocated_to( $admin )
    {
        return Point::where('allocated_to', '=', $admin->id)->get()->sum('amount');
    }

}
