<?php

namespace App\Services\User;

use App\User;
use App\Guardian;

class UserService
{

    // SEARCH FUNSTIONS THAT RETURN SINGLE INSTANCE----------------------------------------------------

    public function get($id)
    {

        $student = User::with('guardian')->find($id);

        return $student;

    }

    public function getByCode($code)
    {
        $student = User::with('guardian')->where('tta_code', '=', $code)->get();

        return $student;

    }

    public function getByCodeAndSubject($code, $subject_id)
    {
        $students = $this->getBySubject($subject_id);

        $student = $students->first(function ($item) use ($code) {

            return $item->tta_code === $code;

        });

        if(!$student)
        {
            return null;
        }

        return $student;
        
    }

    // CRUD ACTION FUNCTIONS----------------------------------------------------------------------------
    
    public function create($data)
    {

        $student = new User();

        $student->name = $data['fullName'];

        $student->email = $data['email'];

        $student->password = bcrypt($data['password']);

        $student->tta_code = $data['ttaCode'];

        $guardian = new Guardian();

        $guardian->name = $data['parent_name'];

        $guardian->email = $data['parent_email'];

        $guardian->password = bcrypt($data['parent_password']);

        $guardian->save();

        $student->guardian()->associate($guardian);

        $student->save();

        $student->subjects()->sync($data['subjects']);

        $student->routines()->sync($data['routines']);

        return $student;

    }

    public function delete($id)
    {

        User::find($id)->subjects()->detach();

        $result = User::destroy($id);

        return $result;

    }

    public function update($data, $id)
    {

        $student = $this->get($id);
        
        $student->update($data);

        if (isset($data['subjects'])) {

            $student->subjects()->sync($data['subjects']);

        }

        if (isset($data['routines'])) {

            $student->routines()->sync($data['routines']);

        }

        if ( isset($data['parent_name']) && isset($data['parent_email']) && isset($data['parent_password']) ) {

            if( $student->guardian ){

               $guardian = $student->guardian; 

               $guardian->name = $data['parent_name']; 

               $guardian->email = $data['parent_email']; 

               $guardian->password = bcrypt($data['parent_password']); 

               $guardian->save();

            }else{

               $guardian = new Guardian();

               $guardian->name = $data['parent_name']; 

               $guardian->email = $data['parent_email']; 

               $guardian->password = bcrypt($data['parent_password']);

               $guardian->save();

               $student->guardian()->associate($guardian);

               $student->save();


            }

        }

        return $student;

    }

    public function updateScore($subject_id, $student_id, $newScore, $action)
    {

        $student = User::find($student_id);

        if ($action == 'increment') {

            $updatedScore = $student->score($subject_id) + $newScore;

            $student->subjects()->detach($subject_id);

            $student->subjects()->attach($subject_id, ['score' => $updatedScore]);

        } elseif (($action == 'decrement')) {

            $updatedScore = $student->score($subject_id) - $newScore;

            if ($updatedScore <= 0) {

                $student->subjects()->detach($subject_id);

                $student->subjects()->attach($subject_id, ['score' => 0]);

            } else {

                $student->subjects()->detach($subject_id);

                $student->subjects()->attach($subject_id, ['score' => $updatedScore]);

            }

        }

        return $student;

    }

    // SEARCH FUNSTIONS THAT RETURNS COLLECTION-------------------------------------------------------------

    public function all()
    {

        $students = User::all();

        return $students;

    }

    public function getBySubject($id)
    {

        $students = User::whereHas('subjects', function ($q) use ($id) {

            $q->where('subject_id', '=', $id);

        })->get();

        return $students;

    }

}
