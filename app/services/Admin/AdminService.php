<?php

namespace App\Services\Admin;

use App\Role;
use App\Admin;

class AdminService
{
    //Admin Tasks
    public function give_from_wallet( $amount, $admin )
    {

        $admin->admin_wallet -= $amount;
        
        $admin->save();

        return;
        
    }

    public function return_to_wallet( $amount, $admin )
    {

        $admin->admin_wallet += $amount;
        
        $admin->save();

        return;
    }

    //Super Admin Tasks
    public function allocate_to_wallet($amount, $receiver_id)
    {
        $receiver = Admin::find($receiver_id);
        $receiver->admin_wallet += $amount;
        $receiver->save();

        auth()->admin()->admin_wallet -= $amount;

        return;
    }

    public function store_admin( $data )
    {

        $admin = new Admin;
        $admin->name = $data['name'];
        $admin->email = $data['email'];
        $admin->password = bcrypt($data['password']);
        $admin->role()->associate(Role::where('name', '=', 'admin')->first());
        $admin->save();

        return $admin;

    }

    public function get_all_admins()
    {
        return Admin::all();
    }

    public function get( $admin_id )
    {
        return Admin::findOrFail( $admin_id );
    }

}
