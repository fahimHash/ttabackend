<?php

namespace App\Services\RoleAndPermission;

use App\Role;
use App\Permission;

class RoleAndPermissionService
{


    public function get_all_roles()
    {
        return Role::all();
    }

    public function get_all_permissions()
    {
        return Permission::all();
    }

    public function get_role( $role_id )
    {
        return Role::findOrFail( $role_id );
    }

    public function update_permission_for_role( $data, $role )
    {
        return Role::find($role)->permissions()->sync($data);
    }
    
    // PERMISSION CHECKERS

    public function check_manage_admin()
    {
        return auth()->user()->can( 'manage-admin' );
    }

    public function check_update_point()
    {
        return auth()->user()->can( 'update-point' );
    }

    public function check_manage_point()
    {
        return auth()->user()->can( 'manage-point' );
    }

    public function check_manage_leaderboard()
    {
        return auth()->user()->can( 'manage-leaderboard' );
    }

    public function check_manage_student()
    {
        return auth()->user()->can( 'manage-student' );
    }

    public function check_populate_section()
    {
        return auth()->user()->can( 'populate-section' );
    }

    public function check_manage_section()
    {
        return auth()->user()->can( 'manage-section' );
    }
}
