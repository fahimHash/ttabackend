<?php

namespace App\Services\Credit;

use App\Credit;

class CreditService
{

    public function CreateForwardCredit( $amount, $to, $subject_id )
    {

        $credit = new Credit();

        $credit->from = auth()->user()->id;

        $credit->to = $to;

        $credit->type = 'forward';

        $credit->amount = $amount;

        $credit->subject_id = $subject_id;

        $credit->save();

        return;
    }

    public function CreateReturnCredit( $amount, $from, $subject_id )
    {

        $credit = new Credit();

        $credit->from = $from;

        $credit->to = auth()->user()->id;

        $credit->type = 'return';

        $credit->amount = $amount;

        $credit->subject_id = $subject_id;

        $credit->save();

        return;

    }

}
