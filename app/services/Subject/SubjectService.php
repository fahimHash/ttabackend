<?php

namespace App\Services\Subject;

use App\Subject;

use App\Section;

use App\Routine;

use \Carbon\Carbon;

class SubjectService
{

    public function create($data)
    {
        $subject = new Subject();

        $subject->title = ucwords($data['title']);

        $subject->save();

        return $subject;
    }

    public function all()
    {
        $subjects = Subject::all();

        return $subjects;
    }

    public function get($id)
    {
        $subject = Subject::find($id);

        return $subject;
    }

    public function update($data, $id)
    {
        $subject = $this->get($id);
        
        $subject->update($data);

        return $subject;
    }

    public function delete($id)
    {   
        Subject::find($id)->users()->detach();

        $subject = Subject::destroy($id);

        return;
    }


    //Section Functions

    public function sections()
    {
        
    }

    public function get_section( $id )
    {
        return Section::with('subject', 'routines')->find($id);
    }

    public function store_section( $name, $subject, $teacher, $day, $start_time, $end_time, $data )
    {

        $section = new Section();

        $section->name = $name;

        $section->subject()->associate( $subject );

        $section->save();

        //routine day 1

        $routine = new Routine();

        $routine->day = $day;

        $routine->start_time = Carbon::parse($start_time)->toDateTimeString();

        $routine->end_time = Carbon::parse($end_time)->toDateTimeString();

        $routine->section()->associate( $section );

        $routine->admin()->associate( $teacher );

        $routine->save();

        //droutine day 2

        if( $data->day_2 && $data->start_time_2 && $data->end_time_2 && $data->teacher_2)
        {

            $routine = new Routine();

            $routine->day = $data->day_2;

            $routine->start_time = Carbon::parse( $data->start_time_2 )->toDateTimeString();

            $routine->end_time = Carbon::parse( $data->end_time_2 )->toDateTimeString();

            $routine->section()->associate( $section );

            $routine->admin_id = $data->teacher_2;

            $routine->save();

        }

        return $section;

    }

    public function section_update( $data, $id )
    {
        
        $section = $this->get_section( $id );

        $section->name = $data->name;

        $section->subject()->associate( $data->subject );

        $section->save();

        return $section;

    }

    public function section_update_with_routine( $data, $id, $routine_data )
    {

        $section = $this->section_update( $data, $id );

        foreach( $routine_data as $routine_id => $values )
        {
            
            $routine = $this->get_routine( $routine_id );

            $routine->day = $values['day'];

            $routine->start_time = $values['start_time'];

            $routine->end_time = $values['end_time'];

            $routine->admin_id = $values['admin_id'];

            $routine->save();

        }

        return $section;

    }

    // Routine Functions

    public function get_routine( $id )
    {

        return Routine::with('users')->find( $id );

    }

}
