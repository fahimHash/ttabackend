<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{   

    protected $fillable = [ 'name' ];
    public $timestamps = false;


    public function admins()
    {
        return $this->hasMany('App\Admin');
    }

    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }
}
