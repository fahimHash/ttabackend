<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{

    protected $fillable = [
        'title'
    ];


    public function users()
    {
        return $this->belongsToMany('App\User')->withPivot('score');
    }

    public function sections()
    {
        return $this->hasMany('App\Section');
    }

    public function routines()
    {
        return $this->hasManyThrough('App\Routine', 'App\Section');
    }
}
