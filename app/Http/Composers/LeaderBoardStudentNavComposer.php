<?php

namespace App\Http\Composers;

use Illuminate\View\View;

class LeaderBoardStudentNavComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {
        // Dependencies automatically resolved by service container...
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {   
        $subjects = auth()->user()->subjects;
        $view->with('subjects', $subjects);
    }
}