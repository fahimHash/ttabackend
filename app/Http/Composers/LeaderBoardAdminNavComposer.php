<?php

namespace App\Http\Composers;

use App\Services\Subject\SubjectService;
use Illuminate\View\View;

class LeaderBoardAdminNavComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $subjectService;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(SubjectService $subjectService)
    {
        // Dependencies automatically resolved by service container...
        $this->subjectService = $subjectService;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {   
        $subjects = $this->subjectService->all();
        $view->with('subjects', $subjects);
    }
}