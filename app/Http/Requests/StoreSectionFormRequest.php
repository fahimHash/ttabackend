<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Services\RoleAndPermission\RoleAndPermissionService;

class StoreSectionFormRequest extends FormRequest
{

    protected $authService;

    public function __construct( RoleAndPermissionService $authService )
    {

        $this->authService = $authService;

    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        
        return $this->authService->check_manage_section();

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'name' => 'required',
            'subject' => 'required|exists:subjects,id',
            'teacher' => 'required|exists:admins,id',
            'day' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Give a nice name for the section'
        ];
    }

}
