<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PointTransferFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required|exists:users,tta_code',
            'subject_id_from' => 'required',
            'subject' => 'required',
            'amount' => 'required|numeric|min:1',
            'comment' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'code.exists' => 'Incorrect TTA Code',
            'code.required' => 'TTA Code is required for the transfer',
            'amount.required' => 'The transfer amount can\'t be Blank',
            'comment.required'  => 'Explain why do you want to transfer your points',
        ];
    }
}
