<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Services\Admin\AdminService;

use App\Services\Credit\CreditService;

use App\Services\Subject\SubjectService;

use App\Services\User\UserService;

use App\Services\RoleAndPermission\RoleAndPermissionService;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class LeaderBoardController extends Controller
{

    protected $studentService;

    protected $subjectService;

    protected $creditService;

    protected $adminService;

    protected $authorize;

    public function __construct( UserService $studentService, SubjectService $subjectService, AdminService $adminService,

        CreditService $creditService, RoleAndPermissionService $authorize ) {

        $this->middleware('auth:admin');

        $this->studentService = $studentService;

        $this->subjectService = $subjectService;

        $this->adminService = $adminService;

        $this->creditService = $creditService;

        $this->authorize = $authorize;

    }

    public function showLeaderBoard_of_a_Subject( $id )
    {

        if( ! $this->authorize->check_manage_leaderboard() ) { return redirect()->back(); } 

        $students = $this->studentService->getBySubject($id);

        $students = $students->sort(function ($a, $b) use ($id) {

            return $a->subjects->firstWhere('id', $id)->pivot->score < $b->subjects->firstWhere('id', $id)->pivot->score;

        });

        $subject = $this->subjectService->get($id);

        return view('admin.leaderboard', compact('students', 'subject'));

    }

    public function increaseScore( Request $request, $subject_id, $student_id )
    {   

        if( ! $this->authorize->check_update_point() ) { return redirect()->back(); }  

        if (auth()->user()->admin_wallet < $request->increment) {

            return redirect()->back()->with(['response_message' => 'You Do Not Have Enough Credit to Give Points']);

        }

        $student = $this->studentService->get($student_id);

        $student = $this->studentService->updateScore( $subject_id, $student->id, $request->increment, 'increment' );

        $this->adminService->give_from_wallet( $request->increment, auth()->user() );

        $this->creditService->CreateForwardCredit( $request->increment, $student_id, $subject_id );

        return redirect()->back()->with(['response_message' => 'updated', 'student' => $student]);

    }

    public function decreaseScore( Request $request, $subject_id, $student_id )
    {   

        if( ! $this->authorize->check_update_point() ) { return redirect()->back(); } 
        
        $student = $this->studentService->get($student_id);

        $student = $this->studentService->updateScore($subject_id, $student->id, $request->decrement, 'decrement');

        $this->adminService->return_to_wallet($request->decrement, auth()->user() );

        $this->creditService->CreateReturnCredit( $request->decrement, $student_id, $subject_id );

        return redirect()->back()->with(['message' => 'updated', 'student' => $student]);

    }

}
