<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use App\Services\Point\PointService;

use App\Services\Admin\AdminService;

use App\Services\RoleAndPermission\RoleAndPermissionService;

class PointController extends Controller
{

    protected $authorize;

    protected $pointService;

    protected $admin_service;

    public function __construct( RoleAndPermissionService $authorize, PointService $pointService, AdminService $admin_service )
    {

        $this->middleware('auth:admin');

        $this->authorize = $authorize;

        $this->pointService = $pointService;

        $this->admin_service = $admin_service;


    }


    public function show_point_generation_page()
    {   

        if( ! $this->authorize->check_manage_point() ) { return redirect()->back(); } 

        $admins = $this->admin_service->get_all_admins();

        return view('admin.GeneratePointForm', compact('admins'));

    }

    public function generate_point()
    {   

        if( ! $this->authorize->check_manage_point() ) { return redirect()->back(); } 
        
        $allocating_by = auth()->user();

        $allocate_to = $this->admin_service->get( request('allocate_to') );

        $point = $this->pointService->create( request('amount'), $allocating_by, $allocate_to );

        $allocate_to->admin_wallet += request('amount');

        $allocate_to->save();

        return redirect()->back()->with(["message" => "{$point->amount} Point has been allocated to {$allocate_to->name}"]);

    }
}
