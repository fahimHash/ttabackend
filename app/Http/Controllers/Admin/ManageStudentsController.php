<?php

namespace App\Http\Controllers\Admin;

use App\User;

use Illuminate\Http\Request;

use App\Services\User\UserService;

use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Services\Subject\SubjectService;

use Yajra\Datatables\Datatables;

use App\Services\RoleAndPermission\RoleAndPermissionService;

class ManageStudentsController extends Controller
{

    protected $studentService;

    protected $subjectService;

    protected $authorize;

   
    public function __construct( UserService $u, SubjectService $s, RoleAndPermissionService $authorize )
    {

        $this->middleware('auth:admin');

        $this->studentService = $u;

        $this->subjectService = $s;

        $this->authorize = $authorize;

    }


    public function newStudentForm()
    {   

        if( ! $this->authorize->check_manage_student() ) { return redirect()->back(); }

        $subjects = $this->subjectService->all();

        return view('admin.newStudentForm', compact('subjects'));

    }

    public function createNewStudent( Request $request )
    {   

        if( ! $this->authorize->check_manage_student() ) { return redirect()->back(); }

        $request->validate( [

            'fullName' => 'required',

            'email' => 'required|unique:users|email',

            'password' => 'required|string|min:8',

            'ttaCode' => 'required|unique:users,tta_code|string|size:5',

            'subjects' => 'required',

            'routines' => 'required',

            'parent_name' => 'required',

            'parent_email' => 'required|unique:guardians,email',

            'parent_password' => 'required',

        ] );
        
        $student = $this->studentService->create($request->only('fullName','email','password','ttaCode', 'subjects', 'routines', 'parent_name', 'parent_email', 'parent_password'));    

        return redirect()->back()->with(['message' => 'success', 'student' => $student]);

    }

    public function showEditForm( $id )
    {

        if( ! $this->authorize->check_manage_student() ) { return redirect()->back(); }

        $student = $this->studentService->get($id);

        $subjects = $this->subjectService->all();

        return view('admin.editStudentForm', compact('student','subjects'));

    }

    public function update( Request $request, $id )
    {
        
        if( ! $this->authorize->check_manage_student() ) { return redirect()->back(); }

        $input = collect( request()->all() )->filter( function($value) {

            return null !== $value;

        } )->toArray();

        if( array_key_exists( "password", $input ) )
        {
            $request->validate( [

                'password' => 'min:8'
                
            ] );

            $input['password'] = bcrypt( $input['password'] );

        }
        
        unset($input['_token']); 
    
        $student = $this->studentService->update( $input, $id );

        return redirect()->back()->with( ['message' => 'success', 'student' => $student] );

    }

    public function manageStudents()
    {

        if( ! $this->authorize->check_manage_student() ) { return redirect()->back(); }

        return view('admin.listStudents');

    }

    public function ajax()
    {
    
        if( ! $this->authorize->check_manage_student() ) { return redirect()->back(); }

        $model = User::with('subjects');

        return Datatables::of($model)

                ->addColumn('action', function($row){

                        $link = route('admin.student.manage.edit', ['id' => $row->id]);

                        $btn = "<a href='{$link}' class='edit btn btn-primary btn-sm'>Edit</a>";

                        return $btn;

                })

                ->make(true);
    }

    public function delete( $id )
    {
        
        if( ! $this->authorize->check_manage_student() ) { return redirect()->back(); }

        $result = $this->studentService->delete($id);

        return redirect()->route( 'admin.students.manage' )->with( ['message' => 'success', 'remove_notice' => 'All data asscociated with This student has been Removed from the Database'] );

    }
    
}
