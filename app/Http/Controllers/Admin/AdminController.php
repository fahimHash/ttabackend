<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Services\RoleAndPermission\RoleAndPermissionService;
use App\Services\Admin\AdminService;
use App\Http\Requests\StoreAdminFormRequest;

class AdminController extends Controller
{


    public $authorize;

    public $admin_service;

    public function __construct(RoleAndPermissionService $authorize, AdminService $admin_service)
    {
        $this->middleware('auth:admin');

        $this->authorize = $authorize;
        
        $this->admin_service = $admin_service;
    }


    public function showDashboard()
    {   
        return view('admin.dashboard');
    }


    /**
     * Admin User managemnt functions.
     */

    public function showManagePage()
    {   
        if( ! $this->authorize->check_manage_admin() ) { return redirect()->back(); }   

        $permissions = $this->authorize->get_all_permissions();
        
        return view('admin.ManageAdmin', compact('permissions') );
    }
    
 
    public function createAdmin(StoreAdminFormRequest $request)
    {   
        if( ! $this->authorize->check_manage_admin() ) { return redirect()->back(); }  

        $admin = $this->admin_service->store_admin(request()->all());

        return redirect()->back()->with(['response_message' => 'New admin created.']);

    }

    /**
     * Role/Permission managemnt functions.
     */

    public function show_manage_select_role_page()
    {
        if( ! $this->authorize->check_manage_admin() ) { return redirect()->back(); }

        $roles = $this->authorize->get_all_roles();

        return view('admin.ManageSelectRole', compact('roles'));
    }

    public function show_manage_role_page()
    {
        if( ! $this->authorize->check_manage_admin() ) { return redirect()->back(); }
        
        $permissions = $this->authorize->get_all_permissions();
        $selected_role = $this->authorize->get_role(request('selected_role'));

        return view('admin.ManageRole', compact('permissions', 'selected_role'));
    }

    public function update_role()
    {
        if( ! $this->authorize->check_manage_admin() ) { return redirect()->back(); }
        
        $result = $this->authorize->update_permission_for_role(request('permissions'), request('role'));
        
        return redirect()->route('admin.settings.role.manage.select')->with(['response_message' => 'Role Updated']);
    }

}
