<?php

namespace App\Http\Controllers\Admin\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
 
    use AuthenticatesUsers;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        
        $this->middleware('guest:admin')->except('logout');
    }


     /**
     * Show the application's login form. [overrriden]
     */
    public function showLoginForm()
    {   
        if(auth()->guard('web')->check()){
            return redirect()->back();
        }

        return view('auth.admin.login');
    }


    /**
     * Log the Admins in. [overrriden]
     */
    public function login(Request $request)
    {   

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) 
        {
            return redirect()->intended(route('admin.dashboard'));
        }

        return redirect()->back()->withInput($request->only('email','remember'));
    }

    /**
     * Log out. [ovverriden].
     */
    public function logout(Request $request)
    {
        $this->guard('admin')->logout();

        $request->session()->invalidate();

        return redirect()->route('admin.login');
    }
    
}
