<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Subject\SubjectService;
use App\Services\Transfer\TransferService;
use App\Services\User\UserService;
use App\Events\PointTransferRequested;
use App\Http\Requests\PointTransferFormRequest;

class TransferController extends Controller
{

    protected $transferService;
    protected $subjectService;
    protected $studentService;

    public function __construct(UserService $studentService, TransferService $transferService, SubjectService $subjectService)
    {
        $this->middleware('auth:admin');
        $this->studentService  = $studentService;
        $this->subjectService  = $subjectService;
        $this->transferService = $transferService;
    }

   
    public function pointTransferRequests()
    {   

        $transfers  = $this->transferService->allPending();
        
        $collection = collect([]);

        foreach( $transfers as $index => $transfer )
        {

            $from         = $this->studentService->get($transfer->from);
            $to           = $this->studentService->get($transfer->to);
            $subject_from = $this->subjectService->get($transfer->subject_id_from);
            $subject_to   = $this->subjectService->get($transfer->subject_id_to);

            $collection->put($index, [
                                        'from'         => $from, 
                                        'to'           => $to, 
                                        'subject_from' => $subject_from, 
                                        'subject_to'   => $subject_to, 
                                        'amount'       => $transfer->amount, 
                                        'comment'      => $transfer->comment, 
                                        'date'         => $transfer->created_at, 
                                        'transfer_id'  => $transfer->transfer_id 
                                    ]
                            );            

        }
    
        return view('admin.TransferRequests', compact('collection'));

    }

    public function approveTransfer(\App\Transfer $transfer)
    {   
        
        $student_who_gave_point = $this->studentService->updateScore(
                                                                        $transfer->subject_id_from, 
                                                                        $transfer->from, 
                                                                        $transfer->amount, 
                                                                        'decrement'
                                                        );

        $student_who_gained_point = $this->studentService->updateScore(
                                                                        $transfer->subject_id_to, 
                                                                        $transfer->to, 
                                                                        $transfer->amount, 
                                                                        'increment'
                                                        );

        $transfer->approved = true;

        $transfer->save();
        
        return redirect()->back();

    }

    public function rejectTransfer(\App\Transfer $transfer)
    {

        $transfer->delete();

        return redirect()->back();
    
    }

}
