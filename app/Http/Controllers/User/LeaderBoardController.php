<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Services\User\UserService;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Services\Subject\SubjectService;

class LeaderBoardController extends Controller
{

    protected $studentService;
    protected $subjectService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserService $studentService, SubjectService $subjectService)
    {
        $this->middleware('auth:web');
        $this->studentService = $studentService;
        $this->subjectService = $subjectService;
    }

    public function showLeaderBoard_of_a_Subject($subject_id)
    {   

        //Get all the students with score more than zero AND Sort the students by score
        $students = $this->studentService->getBySubject($subject_id)->filter(function ($student, $key) use($subject_id){
                            return $student->score($subject_id) > 0;
                        })->
                        sort(
                            function($a, $b) use($subject_id) {
                                return $a->score($subject_id) < $b->score($subject_id);
                            }
                        )->values();
                        
        //Get the rank of the logged in student
        //BUG!!                
        $rank = $students->search(function($student, $key) {
            
            if( $student->id == auth()->user()->id )
            {   
                return $key;
            }
        });
        
        //Take top 3 scrorers
        $students = $students->take(3);
        
        $subject = $this->subjectService->get($subject_id);

        return view('user.leaderboard',compact('students','subject', 'rank'));
    }
    
}
