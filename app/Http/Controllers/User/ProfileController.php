<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\Subject\SubjectService;
use App\Services\User\UserService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Image;

class ProfileController extends Controller
{

    protected $studentService;
    protected $subjectService;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserService $studentService, SubjectService $subjectService)
    {
        $this->middleware('auth:web');
        $this->studentService = $studentService;
        $this->subjectService = $subjectService;
    }

    public function showProfile(Request $request)
    {
        return view('user.profile');
    }

    public function updatePhoto(Request $request)
    {
        if ($request->hasFile('photo')) {
            //delete the existing photo
            $user = auth()->user();

            if ($user->photo != 'https://randomuser.me/api/portraits/lego/1.jpg') {
                $exploded = explode('/', $user->photo);
                $file_to_delete = end($exploded);

                if (env('APP_HOST') == 'local') {
                    File::delete(public_path('\uploads\profile_photos\\' . $file_to_delete));

                } elseif (env('APP_HOST') == 'server') {
                    File::delete(env('APP_PUBLIC_PATH_SERVER') . '/uploads/profile_photos/' . $file_to_delete);
                }

            }

            //get the new photo
            $photo = $request->file('photo');
            $filename = time() . '.' . $photo->getClientOriginalExtension();

            if (env('APP_HOST') == 'local') {
                Image::make($photo)->resize(300, 300)->save(public_path('/uploads/profile_photos/' . $filename));

            } elseif (env('APP_HOST') == 'server') {
                Image::make($photo)->resize(300, 300)->save(env('APP_PUBLIC_PATH_SERVER') . '/uploads/profile_photos/' . $filename);
            }

            //save
            $user->photo = asset('/uploads/profile_photos/') . '/' . $filename;
            $user->save();

            return redirect()->back()->with(['message' => 'Your profile picture has been updated!']);
        }
    }

    public function updateBulk(Request $request)
    {
        $request->validate([
            'email' => 'required|unique:users,email,' . auth()->user()->id . '|email',
            'gender' => 'required',
            'dob' => 'required',
        ]);

        $user = auth()->user();

        $request->dob = Carbon::parse($request->dob);

        $user->email = $request->email;

        if ($request->gender == 'male' || $request->gender == 'female') {
            $user->gender = $request->gender;
        }

        $user->dob = $request->dob;
        $user->save();

        return redirect()->back()->with(['message' => 'Your profile has been updated!']);
    }

}
