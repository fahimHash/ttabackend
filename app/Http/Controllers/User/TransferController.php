<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Services\Subject\SubjectService;
use App\Services\Transfer\TransferService;
use App\Services\User\UserService;
use App\Events\PointTransferRequested;
use App\Http\Requests\PointTransferFormRequest;

class TransferController extends Controller
{

    protected $transferService;
    protected $subjectService;
    protected $studentService;

    public function __construct(UserService $studentService, TransferService $transferService, SubjectService $subjectService)
    {
        
        $this->studentService  = $studentService;
        $this->subjectService  = $subjectService;
        $this->transferService = $transferService;
    }

    public function showTransferPage($subject_id)
    {   

        $subject = $this->subjectService->get($subject_id);
        $subjects = $this->subjectService->all();

        return view('user.transfer', compact('subject', 'subjects'));
    }

    

    public function requestTransfer(PointTransferFormRequest $request)
    {   
        
        $transfarrable = $this->transferService->canThisStudentTransferPoint(
                                                    auth()->user(), 
                                                    $this->subjectService->get(request('subject_id_from')), 
                                                    request('amount') 
                                                );

        if( ! $transfarrable )
        {

            return redirect()->back()->with([
                                            'message'             => 'You can not transfer that much, 
                                                                      Your pending Transfer Amount is alreday too high.', 
                                            'flag_wrong_creds'    => true, 
                                            'comment_field_value' => request('comment')
                                    ]);

        }

        $receiver = $this->studentService->getByCodeAndSubject(request('code'), request('subject'));

        if( ! $receiver )
        {   
            old('code', request('code'));

            return redirect()->back()->with([
                                             'message'             => 'The datail you gave were incorrect! 
                                                                       This student dosen\'t have any class in that subject. 
                                                                       Check again.', 
                                             'flag_wrong_creds'    => true, 
                                             'comment_field_value' => request('comment')
                                        ]);
        }

        $prepared_data = [
            'to'              => $receiver->id, 
            'from'            => auth()->user()->id, 
            'amount'          => request('amount'), 
            'subject_id_from' => request('subject_id_from'),
            'subject_id_to'   => request('subject'),
            'comment'         => request('comment')
        ];

        $newTransfer = $this->transferService->create($prepared_data);

        event(new PointTransferRequested($prepared_data));

        return redirect()->back()->with([
                                        'message' => 'Your request for Point Transfer has been submited for Review.', 
                                        'flag_awaits_approval' => true
                                   ]);
        
    }


}
