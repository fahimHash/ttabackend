<?php

namespace App\Http\Controllers\Subject;

use App\Subject;

use App\Section;

use Illuminate\Http\Request;

use App\Http\Requests\StoreSectionFormRequest;

use Yajra\Datatables\Datatables;

use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

use App\Services\Subject\SubjectService;

use App\Services\Admin\AdminService;

class SubjectController extends Controller
{

    protected $subjectService; 

    protected $adminService; 

    public function __construct( SubjectService $subjectService, AdminService $adminService )
    {

        $this->middleware('auth:admin');

        $this->subjectService = $subjectService;

        $this->adminService = $adminService;

    }

    public function newSubjectForm()
    {   

        return view('admin.newSubjectForm');

    }

    public function createNewSubject( Request $r )
    {

        $subject = $this->subjectService->create($r->only('title'));

        return redirect()->back()->with(['message' => 'success', 'subject' => $subject]);

    }
    
    public function ajax()
    {

        $model = Subject::query();

        return Datatables::of($model)
                ->addColumn('action', function($row){

                        $link = route('admin.subject.manage.edit', ['id' => $row->id]);

                        $btn = "<a href='{$link}' class='edit btn btn-primary btn-sm'>Edit</a>";

                        return $btn;
                })
                ->make(true);
    }

    public function showEditForm($id)
    {

        $subject = $this->subjectService->get($id);

        return view('admin.editSubjectForm',compact('subject'));

    }

    public function update(Request $request,$id)
    {

        $input = collect(request()->all())->filter(function($value) {

            return null !== $value;

        })->toArray();

        unset( $input['_token'] );
        
        if( empty($input) )
        {  

            return redirect()->back()->with(['invalid_notice' => 'Empty Data is not Accepted']);

        }
        
        $subject = $this->subjectService->update($input, $id);

        return redirect()->back()->with(['message' => 'success', 'subject' => $subject]);

    }

    public function delete($id)
    {
        $this->subjectService->delete($id);

        return redirect()->route('admin.subject.new')->with(['delete_notice' => 'This subject and any associated data with it has been removed']);

    }

    //SECTION FUNCTIONS

    public function new_section_form()
    {
        $subjects = $this->subjectService->all();

        $admins = $this->adminService->get_all_admins();

        return view( 'admin.newSectionForm', compact('subjects', 'admins') );

    }

    public function store_section(StoreSectionFormRequest $request)
    {

        $subject = $this->subjectService->get( request()->subject );

        $teacher = $this->adminService->get( request()->teacher );

        $section = $this->subjectService->store_section( request()->name, $subject, $teacher, request()->day, request()->start_time, 
            
            request()->end_time, $request );

        return redirect()->back()->with([ "response_message" => "{$section->name} has been successfully created." ]);

    }

    public function section_ajax()
    {
        
        $model = Section::with('subject')->get([ 'id','name','subject_id' ]);

        return Datatables::of($model)

                ->addColumn('subject', function($row){

                    return $row->subject->title;

                })

                ->addColumn('action', function($row){

                        $link = route('admin.section.manage.edit', ['id' => $row->id]);

                        $btn = "<a href='{$link}' class='edit btn btn-primary btn-sm'>Edit</a>";

                        return $btn;
                })

                ->removeColumn('subject_id')

                ->make(true);

    }


    public function show_section_edit_form( $id )
    {
    
        $section = $this->subjectService->get_section($id);
        
        $subjects = $this->subjectService->all();

        $admins = $this->adminService->get_all_admins();
        
        return view( 'admin.editSectionForm',compact( 'section', 'subjects', 'admins' ) );

    }

    public function section_update_with_routine( Request $request, $section_id )
    {   
        
        $routine_data_to_update = collect();
        
        foreach( $request->routine_id as $routine_id )
        {   

            $routine_data_to_update[ $routine_id ] = [

                'day' => $request["day_{$routine_id}"],

                'start_time' => $request["start_time_{$routine_id}"],
                
                'end_time' => $request["end_time_{$routine_id}"],

                'admin_id' => $request["teacher_{$routine_id}"]

            ];

        }

        $section = $this->subjectService->section_update_with_routine( $request , $section_id, $routine_data_to_update );

        return redirect()->back()->with([ "response_message" => "{$section->name} has been successfully updated." ]);

    }


}
