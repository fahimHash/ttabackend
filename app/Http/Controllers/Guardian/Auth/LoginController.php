<?php

namespace App\Http\Controllers\Guardian\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
 
    use AuthenticatesUsers;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {   
        
        $this->middleware('guest:guardian')->except('logout');
    }


     /**
     * Show the application's login form. [overrriden]
     */
    public function showLoginForm()
    {   
        if( auth()->guard('web')->check() || auth()->guard('admin')->check() || auth()->guard('guardian')->check() )
        {

            return redirect()->back();

        }

        return view('auth.guardian.login');
    }


    /**
     * Log the Parent in. [overrriden]
     */
    public function login(Request $request)
    {   

        if (Auth::guard('guardian')->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->intended(route('guardian.dashboard'));
        }

        return redirect()->back()->withInput($request->only('email','remember'));
    }

    /**
     * Log out. [ovverriden].
     */
    public function logout(Request $request)
    {
        $this->guard('guardian')->logout();

        $request->session()->invalidate();

        return redirect()->route('guardian.login');
    }
    
}
