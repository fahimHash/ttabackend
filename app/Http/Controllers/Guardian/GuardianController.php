<?php

namespace App\Http\Controllers\Guardian;

use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Services\RoleAndPermission\RoleAndPermissionService;
use App\Services\Admin\AdminService;
use App\Http\Requests\StoreAdminFormRequest;

class GuardianController extends Controller
{


    public $authorize;

    public $admin_service;

    public function __construct(RoleAndPermissionService $authorize, AdminService $admin_service)
    {
        $this->middleware('auth:guardian');

        $this->authorize = $authorize;
        
        $this->admin_service = $admin_service;
    }


    public function showDashboard()
    {   
        return view('guardian.dashboard');
    }

}
