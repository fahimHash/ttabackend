<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Routine extends Model
{

    public $timestamps = false;

    public function section()
    {

        return $this->belongsTo('App\Section');

    }

    public function admin()
    {

        return $this->belongsTo('App\Admin');

    }

    public function users()
    {

        return $this->belongsToMany('App\User');

    }
}
