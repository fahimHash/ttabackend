<?php

namespace App\Providers;

use App\Role;
use App\Permission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class PermissionServiceProvider extends ServiceProvider
{   

    public function boot()
    {
        
        Permission::get()->map( function ( $permission ) {

            Gate::define( $permission->name, function ($user) use ($permission) {

                return $user->hasPermissionTo($permission);

            } );

        });
        
    }
}
