<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class LeaderBoardViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            'partials.nav.admin', 'App\Http\Composers\LeaderBoardAdminNavComposer'
        );

        
        View::composer(
            'partials.nav.user', 'App\Http\Composers\LeaderBoardStudentNavComposer'
        );
    }
}
