<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }
}
