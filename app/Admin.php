<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;

    protected $guard = 'admin';



    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];



    
    public function points()
    {

        return $this->hasMany('App\Point');

    }

    public function role()
    {

        return $this->belongsTo('App\Role');

    }

    public function routines()
    {

        return $this->hasMany('App\Routine');

    }


    public function hasPermissionTo( $permission )
    {      
               
        return $this->role->permissions->contains( $permission );

    }
}
