<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routines', function (Blueprint $table) {
            
            $table->bigIncrements('id');

            $table->string('day');

            $table->time('start_time');

            $table->time('end_time');

            $table->integer('section_id')->unsigned();

            $table->foreign('section_id')->references('id')->on('sections')->onDelete('cascade');

            $table->integer('admin_id')->unsigned();

            $table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routines');
    }
}
