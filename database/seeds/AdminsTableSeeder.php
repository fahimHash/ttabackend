<?php

use Illuminate\Database\Seeder;
use App\Admin;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::truncate();

        $admin = new Admin();
        $admin->name = 'Shams';
        $admin->email = 'shams@mail.com';
        $admin->password = bcrypt('shams1123');
        $admin->save();

    }
}
