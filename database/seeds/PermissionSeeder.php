<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permissions = Permission::all();

        //Points
        if ( ! $permissions->contains('name','update-point') ) { Permission::create([ 'name' => 'update-point' ]); }

        if ( ! $permissions->contains('name','manage-point') ) { Permission::create([ 'name' => 'manage-point' ]); }

        //Leaderboard
        if ( ! $permissions->contains('name','manage-leaderboard') ) { Permission::create([ 'name' => 'manage-leaderboard' ]); }

        //Subject
        if ( ! $permissions->contains('name','manage-subject') ) { Permission::create([ 'name' => 'manage-subject' ]); }

        //Section
        if ( ! $permissions->contains('name','populate-section') ) { Permission::create([ 'name' => 'populate-section' ]); }

        if ( ! $permissions->contains('name','manage-section') ) { Permission::create([ 'name' => 'manage-section' ]); }

        //Student
        if ( ! $permissions->contains('name','manage-student') ) { Permission::create([ 'name' => 'manage-student' ]); }

        //Admin
        if ( ! $permissions->contains('name','manage-admin') ) { Permission::create([ 'name' => 'manage-admin' ]); }

    }
}
