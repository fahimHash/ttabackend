<?php

use Illuminate\Database\Seeder;
use App\Permission;
use App\Role;

class RolePermissionSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {   
        
        $role_su = Role::where('name','super-admin')->first();
        $role_su->permissions()->detach();
        $role_su->permissions()->attach(Permission::all());
        
        // $role_ad = Role::where('name','admin')->first();
        // $role_ad->permissions()->detach();
        // $role_ad->permissions()->attach(Permission::where('name','=','update-point')
        //                                             ->orWhere('name','=','manage-leaderboard')
        //                                             ->orWhere('name','=','manage-student')->get()
        //                                 );

    }
}
