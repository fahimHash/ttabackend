<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::all();

        if ( ! $roles->contains('name','super-admin') ) { Role::create([ 'name' => 'super-admin' ]); }
        if ( ! $roles->contains('name','admin') ) { Role::create([ 'name' => 'admin' ]); }
    }
}
